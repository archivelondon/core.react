import React, { Component } from 'react';
import {withStore} from '../../utils/Store';

import {
  Link
} from 'react-router-dom';

import '../../helpers/String';

/*
	Arbitrary storing of data on click
	store.store('name',value)

	use <DataLink name value />

	- or -

	Hash routing for data variables. Can be expanded by DataRouter
	/#/name/value+value-2+value-3/name-2/value/
	
	use <DataLink name value route />


*/

class DataLink extends Component {

	active = false;

	constructor(props) {
		super(props);
		this.state = {};
	}

	componentWillMount() {
		


	}

	componentWillUnmount() {

	}



	doClick = (key,data) => {

		if(this.props.reset){
			this.props.store.unstore(key,data);
			return;
		}
		if(this.props.multi){

			if(this.props.toggle && this.active){
				this.props.store.unmerge(key,data);
			}else{

				this.props.store.merge(key,data);

			}

		}else{

			if(this.props.toggle && this.active){
				this.props.store.unstore(key,data);
			}else{

				this.props.store.store(key,data);

			}

		}

	}

	baseUri = () => {

		let hash = window.location.hash.replace('#/','');
		hash = '#/'+hash;
		let base = (this.props.base) ? this.props.base : hash;

		return base;

	}

	getSlug = (value) => {

		let slug = this.props.slug || value.toString().replace(/%s/gi,'-');
		return slug;
	}

	getActiveKey = (key,base) => {

		let pattern = key+'/([^/]+)/';
		let regExp =  new RegExp(pattern,"i");
		let uri = (this.props.absolute) ? window.location.href : base;
		let activeKey = regExp.exec(uri);

		return activeKey;

	}

	isActive = (key,value,base) => {

		let activeKey = this.getActiveKey(key,base);
		
		let slug = this.getSlug(value);
			let activeValues;
			let active = false;


			// Something for this data variable is in the URI...does it match this item?
			if(activeKey){

				// e.g. /category/coffee/ or /category/coffee+tea+skimmed-milk/
				activeValues = activeKey[1].split('+');
				active = activeValues.find(item=>(item==slug));
			}

			return active;

	}

	// Remove a whole segment e.g. -> /categories/a+b+c/ <- from the URI
	resetUriKey = (key,base) => {

		let activeKey = this.getActiveKey(key,base);
		let uri = base;

		if(activeKey){

			uri = (this.props.absolute) ? base + '/' : base.replace(activeKey[0],'');

		}

		return uri;

	}

	// Remove a single value from a segment /categories/a+ -> b <- +c/ from the URI
	unMergeUriValue = (key,value,base) => {

		let activeKey = this.getActiveKey(key,base);
		
		let slug = this.getSlug(value);
			let activeValues;
			let active = false;

			let outValues = [];

			// Something for this data variable is in the URI...does it match this item?
			if(activeKey){

				// e.g. /category/coffee/ or /category/coffee+tea+skimmed-milk/
				activeValues = activeKey[1].split('+');
				activeValues.forEach(v=>{

					// Not this value, can leave it in...
					if(v != slug) outValues.push(v);

				})

				if(outValues.length){

					return (this.props.absolute) ? (base + '/' + key + '/' + outValues.join('+')) : base.replace(activeKey[1],outValues.join('+'));

				}else{

					// No values left...remove the segment
					return (this.props.absolute) ? base + '/' : base.replace(activeKey[0],'');

				}
				
			}else{

				// Not active, return the base
				return base;

			}


	}

	// Insert a single value into a segment /categories/a+ -> b <- +c/ from the URI
	mergeUriValue = (key,value,base) => {

			let activeKey = this.getActiveKey(key,base);
		
			let slug = this.getSlug(value);
			let activeValues;
			let active = false;

			let outValues = [];

			// Something for this data variable is in the URI...does it match this item?
			if(activeKey){

				// e.g. /category/coffee/ or /category/coffee+tea+skimmed-milk/
				activeValues = activeKey[1].split('+');
				activeValues.push(slug);

				return (this.props.absolute) ? base + '/' + key + '/' + activeValues.join('+') : base.replace(activeKey[1],activeValues.join('+'));

				
				
			}else{

				// Segment not active, return the key and value
				return base + '/' + key + '/' + slug + '/';

			}

	}

	// Remove a single value from a segment /categories/-> b <-/ from the URI
	unSetUriValue = (key,value,base) => {

		let activeKey = this.getActiveKey(key,base);	
			let slug = this.getSlug(value);


			// Something for this data variable is in the URI...does it match this item?
			if(activeKey){

				return (this.props.absolute) ? base : base.replace(activeKey[0],'');	
				
			}else{

				// Segment not active, return the key and value
				return base + '/' + key + '/' + slug + '/';

			}

	}

	// Insert a single value into a segment /categories/-> b <-/ from the URI
	setUriValue = (key,value,base) => {

		let activeKey = this.getActiveKey(key,base);
		let slug = this.getSlug(value);


			// Something for this data variable is in the URI...
			if(activeKey){



				return (this.props.absolute) ? base + '/' + key + '/' + slug + '/' : base.replace(activeKey[1],slug);

				
			}else{

				// Segment not active, return the key and value
				return base + '/' + key + '/' + slug + '/';

			}


	}

	makeUri = (key,value) => {

		let base = this.baseUri();
		let uri ='';
		let isActive = this.isActive(key,value,base);

		if(this.props.reset){
			uri = this.resetUriKey(key,base);
			
		}else{
		if(this.props.multi){

			if(this.props.toggle && isActive){
				uri = this.unMergeUriValue(key,value,base);
			}else{

				uri = this.mergeUriValue(key,value,base);

			}

		}else{

			if(this.props.toggle && isActive){
				uri = this.unSetUriValue(key,value,base);
			}else{

				uri = this.setUriValue(key,value,base);

			}

		}
		}

		uri = uri.replace(/\/\//gi,'/');

		if(!uri) uri =  '/';
		if(uri == '#/') uri = window.location.pathname;

		

		return {uri:uri,active:isActive};

	}

	/*

		RENDER

	*/

	render() {

		let _this = this;
		let name = this.props.name;
		let className=this.props.className;
		if(!className) className = '';

		if(this.props.route){


			let hash = window.location.hash;
			let base = (this.props.base) ? this.props.base : hash;

			// Get the segment name e.g. /category/
			let segment = this.props.segment || this.props.name;
			let value = (this.props.value) ? this.props.value : '';

			let made = this.makeUri(segment,value);
			let uri = made.uri;

			if(!this.props.reset){
				if(this.props.active){
					className += ' is-active';
				}else{
					className += (made.active) ?' is-active' : ' is-inactive';
				}
				
			}

			

			return <Link to={uri} className={className} >{this.props.children}</Link>
			
			/*
			// What is the value of this link (either passed in or already in data)?
			let value = (this.props.value) ? this.props.value : this.props.store.get(this.props.name);
			if(!value) value = '';
			


			let uri;


			let slugName = this.props.name;
			let slug = this.props.slug || value.toString().replace(/%s/gi,'-');
			

			let pair = slugName+'/'+slug+'/';

			

			let remove = this.props.reset || (this.props.toggle && base.match(pair));

			uri = (remove) ? base.replace(pair,'') : base+'/'+pair+'/';

			uri = uri.replace(/\/\//gi,'/');

			//if(this.props.reset)

			return <NavLink to={uri}>{uri}</NavLink>
		*/
			

		}

		let active = false;

		if(this.props.data.hasOwnProperty(this.props.name) && this.props.value){
			//console.log('DATALINK',this.props.name,this.props.data[this.props.name],this.props.value);

			if(this.props.store.equals(this.props.name,this.props.value)){

				active = true;
			}

			if(this.props.multi){

				if(this.props.store.contains(this.props.name,this.props.value)){

					active = true

				}

			}

		}

		this.active = active;

		if(!this.props.reset){
			className += (active) ?' is-active' : ' is-inactive';
		}

		let propsToCopy = {};

		if(this.props.onMouseEnter) propsToCopy.onMouseEnter = this.props.onMouseEnter;
		if(this.props.onMouseLeave) propsToCopy.onMouseLeave= this.props.onMouseLeave;
		if(this.props.title) propsToCopy.title= this.props.title;

        return <a {...propsToCopy} className={className} onClick={() => (this.doClick(this.props.name,this.props.value))}>{this.props.children}</a>;
		

	}
}

export default withStore(DataLink);