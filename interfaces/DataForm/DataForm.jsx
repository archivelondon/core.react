import React, { Component } from 'react';

import {Form} from '../../components';
import {withStore} from '../../utils/Store';


class DataForm extends Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	componentWillUpdate() {
		
		let _this = this;

	}

	componentWillUnmount() {

	}

	dataStore = (key,data) => {
		this.props.store.store(key,data);

	}

	/*

		RENDER

	*/

	render() {

		return (<Form {...this.props} onChange={(data) => this.dataStore(this.props.name,data)} fields={this.props.fields} />)
		

	}
}

export default withStore(DataForm);