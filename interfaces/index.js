import DataForm from './DataForm';
import DataLink from './DataLink';

export {
    DataLink,
    DataForm
};
