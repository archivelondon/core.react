import React from 'react'

export default {
    title: "Loader",
    stories: [
      {
      name: "Loader",
      props: {
        manifest: ['https://static.photocdn.pt/images/articles/2017_1/iStock-545347988.jpg','https://42mzqz26jebqf6rd034t5pef-wpengine.netdna-ssl.com/wp-content/uploads/2013/02/landscape_taxonomy_04.jpg','https://static.pexels.com/photos/414171/pexels-photo-414171.jpeg'],
        children : <div><img width="200" src="https://static.photocdn.pt/images/articles/2017_1/iStock-545347988.jpg" /><img width="200" src="https://42mzqz26jebqf6rd034t5pef-wpengine.netdna-ssl.com/wp-content/uploads/2013/02/landscape_taxonomy_04.jpg" /></div>
      }
    }
    ]
}

