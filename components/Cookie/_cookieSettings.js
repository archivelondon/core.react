
let cookieSettings = {};

cookieSettings.AGREE_KEY = 'optionalCookiesAllowed';
cookieSettings.AGREE_VALUE = 'YES';
cookieSettings.DECLINE_VALUE = 'NO';
cookieSettings.PREFERENCES_URL = '/cookies/';

export default cookieSettings;
