import React, { Component } from 'react';
import {TimelineMax} from 'gsap';

import MouseWheel from '../../utils/MouseWheel';


class Scroller extends Component {

  length = 0;
  pos = 0;
  tl;

  constructor(props) {
    super(props);
    this.state = {progress:0};
  }

  componentDidMount() {
    
        this.init();
        window.addEventListener('resize',this.resize);

  }

  componentWillUnmount() {

    this.destroy();
    window.removeEventListener('resize',this.resize);

  }

  init = () =>{

    const tl = new TimelineMax({paused:true});
        this.tl = tl;

        this.length = this.refs.mouseWheel.refs.el.offsetHeight;

        if(this.props.onInit) this.props.onInit(this);

  }

  resize = () => {

    if(this.props.onResize){
        this.props.onResize(this);
        return;
    }

    // Store previous progress
    let posWas = this.pos;
    let progressWas = this.progress;
    this.tl.progress(0);
    

    // Re initialise the scroller
    this.destroy();
    this.init();

    // Update the position as it was
    this.pos = this.length * progressWas;
    this.update();


  }

  setLength = (len) => {

    this.length = len;
    this.progress = this.pos/this.length;

  }

  destroy = () => {

    if(this.tl) this.tl.kill();

  }

  mouseWheel = ({delta,e}) => {

      let pos = this.pos + delta;

      if(pos < 0 ) pos = 0;
      if(pos > this.length) pos = this.length;

      this.pos = pos;


      this.update();

    }

    update = () => {

      

      const scroll = this.pos;
      const dist = this.length;

      let progress = (scroll/dist);



      if(progress < 0) progress = 0;

      if(progress != this.progress){
         
         this.progress = progress;

         // Seek the animation
         this.tl.progress(progress);
        



      if(this.props.onUpdate) this.props.onUpdate(this);
      
      
        }


    }

      


    

  /*
    
    RENDER

  */

  render() {

    return (
      <MouseWheel {... this.props} ref="mouseWheel" onUpdate={this.mouseWheel} >
        { this.props.children }
      </MouseWheel>
    );

  }
}

export default Scroller;
