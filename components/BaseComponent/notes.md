Use as a base component where you need a family name and modifiers

```
<BaseComponent family={Item} modifers={['big','alt']}>
	<div>Content</div>
</BaseComponent>
```
Output:
```
<div class="Item Item--big Item--alt">
	<div>Content</div>
</div>
```