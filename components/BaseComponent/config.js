import React, {Component} from 'react'

export default {
	title: "BaseComponent",
	stories: [
		{
			name: "Item",
			props: {
				family: 'Item',
				modifiers: ['modified'],
				children : <div><h1>Item</h1><p>Item body</p></div>
			}
		},
		{
			name: "Grid",
			props: {
				family: 'Grid',
				modifiers: ['1','2'],
				children : <div><div>Grid item 1</div><div>Grid Item 2</div></div>
			}
		}
	]
}

