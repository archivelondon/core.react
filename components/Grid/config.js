import React, {Component} from 'react'

export default {
	title: "Grid",
	stories: [
		{
			name: "Grid--2",
			props: {
				modifiers: ['2'],
				children : <div className="Grid-item">Grid-item</div>
			}
		},
		{
			name: "Grid--4",
			props: {
				modifiers: ['4'],
				children : <div className="Grid-item">Grid-item</div>
			}
		}
	]
}

