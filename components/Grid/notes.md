Output an item with modifiers
```
<Item modifers={['big','alt']}>
	<div>Content</div>
</Item>
```
Output:
```
<div class="Item Item--big Item--alt">
	<div>Content</div>
</div>
```