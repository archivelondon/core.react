import React, { Component } from 'react';

import Item from '../../components/Item'
import Icon from '../../components/Icon'
import Image from '../Image'


import '../../helpers/Array'

class Instagram extends Component {

	constructor(props) {
    super(props);

  }



  render() {

    let limit = this.props.limit || 12;

  	if(!this.props.items)  return null;
    return (
      <Item modifiers={['instaFeed']}>
      	<div className="Item-head">
      		<a className="Link Link--social" href="https://www.instagram.com/thefenway/" target="_blank"><Icon type="instagram-white" /></a>
      	</div>
      	<div className="Item-body">
      	<div className="Grid Grid--6 Grid--tiles  Grid--instagram">


 		{ this.props.items.limit(limit).map((item) =>

 			(
 				<div className="Grid-item">
 					<Item modifiers={['square']}>
 						<div className="Item-media"><Image className="Image--fluid Image--grayscale" src={item.low_resolution} /></div>
 					</Item>
 				</div>
 			)

 		) }
 		</div>
 		</div>
      </Item>
    );
  }
}

export default Instagram;
