import React, {Component} from 'react'
import Site, {SiteModifier_,withSite} from '../../utils/Site';

import moment from 'moment';
import 'moment-timezone';

class Days extends Component {

	monent;	

	componentDidMount = () => {


	}

	itemsForDay = ({day,items,compareField,timeZone}) => {

		// Date can either be a string or object {date:[string]}

		return items.filter((item) => {

			let dateStr = (typeof item[compareField] == 'string') ? item[compareField] : item[compareField].date;
			let compare = moment.tz(dateStr,timeZone);
			let different = (compare.format('YYYY-MM-DD') != day.format('YYYY-MM-DD'));


			//console.log('COMPARE',item.title,compare.format('YYYY-MM-DD'),day.format('YYYY-MM-DD'),!different);

			if(!different) return item;

		});


	}
  render () {

  	let _this = this;
  	let timeZone = this.props.timezone || 'Europe/London';
  	let showDays = this.props.days || 14; // Default to 14 days
  	let showFrom = (this.props.from) ? moment.tz(this.props.from,timeZone) : moment.tz(timeZone);
  	let showTo = (this.props.to) ? moment.tz(this.props.to,timeZone) : false;

  	// Field on each item to compare
  	let compareField = (this.props.compare) || 'startDate';
  	let showEmpty = (this.props.showEmpty === false) ? this.props.showEmpty : true;


  	let defaultLabels = {
	    sameDay: '[Today]',
	    nextDay: '[Tomorrow]',
	    nextWeek: 'dddd',
	    lastDay: '[Yesterday]',
	    lastWeek: '[Last] dddd',
	    sameElse: 'ddd MM.DD'
	}



  	let labels = this.props.labels || defaultLabels;

  	let {items} = this.props;

    let direction = this.props.direction !== null ? this.props.direction : 1;

    console.log('Direction',direction)

  	// America/New_York
  	// https://momentjs.com/timezone/
  	


  	this.moment = showFrom.clone();

  	showFrom.endOf('day');

  	let renderIx = 0;
  	let itemIx = 0;

    return (
    	
    	[...new Array(showDays)].map((i,ix) => {

    		let day = direction ? showFrom.clone().add(ix, 'day') : showFrom.clone().subtract(ix, 'day');
    		let itemsForDay = this.itemsForDay({day,items,compareField,timeZone});

        //console.log('itemsForDay',items)
			if(!showEmpty && !itemsForDay.length) return false;

    		let callbackData = {

    			from : showFrom.clone(), // Start of the set moment object
    			day : day.clone(), // The day moment object
    			label : day.calendar(showFrom,labels), // Label of the day
    			items : itemsForDay, // Items for the day
    			moment : this.moment.clone(), // Time right now in the timezone
    			ix : renderIx
    		}

    		renderIx = (showEmpty || itemsForDay.length ) ? (renderIx + 1) : renderIx;
    		

    		

    		if(this.props.render){
    			let rendered = _this.props.render(callbackData,renderIx);
          itemIx++;
    			if(rendered) return {...rendered,key:itemIx-1}
    		
    		}

    		//return (<div className="Content"><p>{day.calendar(showFrom,labels)} {day.format('dddd, MMMM Do YYYY, h:mm:ss a')} {this.moment.format('dddd, MMMM Do YYYY, h:mm:ss a')}</p></div>)

    	})
    
    )
  }
}

export default Days