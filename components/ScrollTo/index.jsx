import ScrollTo from './ScrollTo';
import {ScrollUpdateURI} from './ScrollTo';

export {ScrollUpdateURI,ScrollTo};
export default ScrollTo;
