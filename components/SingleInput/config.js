import React, {Component} from 'react'

export default {
    title: "SingleInput",
    stories: [
      {
      name: "SingleInput",
      props: {
        "title":"Search",
        "className":"Form-input",
        "name":"q",
        "inputType":"search",
        "value": "",
        "placeholder":"Search & hit enter",
        "disabled":false,
        "onChange":false
      },
    }
    ],
}
