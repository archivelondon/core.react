import React, {Component} from 'react'

import _coreStories from '../../../_coreStories.js';

const utils = _coreStories.listUtils

const arr = [];

utils.map((util,ix) => {
    arr.push({
      name: util,
      props: {
        key: ix,
        modifiers: [util],
        children: <div><li className="List-item">List item</li><li className="List-item">List item</li></div>
      }
    })

});


export default {
    title: "List",
    stories: arr
}

