# StoryWrapper

This wrapper automatically includes a router, loader and site class so that links and modifiers will work for any story item passed in as children.

Any components that rely on a `<Data>` component can work if included this way, or the story can use the `onLoad` callback function to reference manifest data as part of the story.

```
// How to ceate a story.jsx file which has access to manifest data, site and routes
// File: story.jsx

/*

Props:
manifest: array of files to preload into the global data
story: config for this particular story (e.g. specific props to use, name and label for the story)
render: the REACT component to render once loaded

*/

import React from 'react'
import { storiesOf } from '@storybook/react'
import { withNotes } from '@storybook/addon-notes'
import { action } from '@storybook/addon-actions'

import Config from './config'
import Notes from './notes.md'
import Component from './index'

import settings from '../../_settings.js';
import {StoryWrapper} from '../../_core/components';

Config.stories.map((story) => {

storiesOf(Config.title, module)

  .add(Comp.name, withNotes(Notes) (() =>
    <StoryWrapper manifest={settings.STORY_MANIFEST} story={story} render={Component} />
  ));

});

```

# Stories can pass basic props or can pass an onLoad callback to query the manifest data before rendering

```
// Example story config
{
	title: "MyComponent",
	stories: [
		{
			name: "MyComponent Dynamic",
			props: {},
			onLoad : (MyComponent,manifest,props) => {

				let {entries} = manifest;
				// Return a <MyComponent /> using the first manifest entry as the render props
				return <MyComponent {...entries[0]} />


			}
		},
		{
			name: "MyComponent",
			// Use the supplied props object as the render props
			props: {
				title: 'The title',
				otherProp: 'Something else'
			}
		}
	]
}


```
