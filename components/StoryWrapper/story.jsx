import React from 'react'
import { storiesOf } from '@storybook/react'
import { withNotes } from '@storybook/addon-notes'
import { action } from '@storybook/addon-actions'

import StoryRouter from 'storybook-router';

import Config from './config'
import Notes from './notes.md'
import Component from './index'

Config.stories.map((Comp) => {

storiesOf(Config.title, module)
	.addDecorator(StoryRouter())
  	.add(Comp.name, withNotes(Notes) (() =>
    	<Component {...Comp.props} />
  	));

});
