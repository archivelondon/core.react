Core layout directives, for rapid prototyping layouts.
Must include the layout components in the SASS

```
Wrappers:
<div data-layout></div>
<div data-layout="fluid"></div>
<div data-layout="window"></div>
<div data-layout="fixed"></div>

Absolute elements:
<div data-layout-head></div> Goes along the top 100%
<div data-layout-foot></div> Goes along the bottom 100%
<div data-layout-left></div> Goes down the left 100%
<div data-layout-right></div> Goes down the right 100%
<div data-layout-centre></div> Absolute centre
<div data-layout-bg></div> Fills the background
<div data-layout-tl></div> Top left
<div data-layout-tr></div> Top right
<div data-layout-bl></div> Bottom left
<div data-layout-br></div> Bottom right
<div data-layout-tm></div> Halfway across the top
<div data-layout-bm></div> Halfway across the bottom
<div data-layout-lm></div> Halfway down the left
<div data-layout-rm></div> Halfway down the right

Nested elements
<div data-layout-body></div> Just natural
<div data-layout-container></div> Follow SASS max width container setings
<div data-layout-middle><div data-layout-body></div></div> Vertically aligns content
<div data-layout-scroll></div> Scrollable region

Layers:
<div data-layout-front></div> Z index of 5
<div data-layout-back></div> Z index of 0


Usage:

Basic:

<Layout>
	<div data-layout-container>Container</div>									
</Layout>



Window:
Make a panel the size of the viewport

<Layout type="window">
	<div data-layout-container>Container</div>									
</Layout>



Vertically align centre with scroll:
Align a content block to the middle of the element

<Layout type="window">
	<div data-layout-middle data-layout-scroll>
	<div data-layout-body>
		<div data-layout-container>
			Container
		</div>
	</div>
	</div>								
</Layout>



Toolbar along the top:

<Layout type="window">
	<div data-layout-head>
		<div data-layout-container>Head</div>
	</div>
	<div data-layout-body>
		<div data-layout-container>
			Container
		</div>
	</div>
	</div>								
</Layout>
```
