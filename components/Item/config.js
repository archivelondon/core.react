import React, {Component} from 'react'

export default {
	title: "Item",
	stories: [
		{
			name: "Item",
			props: {
				modifiers: [],
				media : [{ url: "/ui/img/placeholder/placeholder.png", "fluid": false }],
				fluid : false,
				linkClass: "Link  Link--button",
				linkText: "Book",
				content: <div><h2>T Rextasy</h2><h5>Friday 19 January — 7.30pm</h5></div>
			}
		},
		{
			name: "Item Call To Action",
			props: {
				modifiers: ['callToAction'],
				media : [{ url: "/ui/img/placeholder/placeholder2.png", "fluid": false }],
				fluid : false,
				linkClass: "Link  Link--callToAction",
				linkText: "Hotels"
			}
		},
		{
			name: "Item Intro",
			props: {
				modifiers: ['intro'],
				media : [{ url: "/ui/img/placeholder/placeholder3.png", "fluid": false }],
				fluid : true,
				linkClass: "Link  Link--button-big",
				linkText: "What's On",
				contentModifier: "intro",
				content: <div><h1>Katherine Ryan: Glitter Room</h1><p>Did you know you can now hire the Assembly Hall</p></div>
			}
		},
		{
			name: "Item Hub",
			props: {
				modifiers: ['hub'],
				media : [{ url: "/ui/img/placeholder/placeholder.png", "fluid": false }],
				fluid : false,
				linkClass: "Link  Link--underline",
				linkText: "More",
				contentModifier: "hub",
				content: <div><h3>Youth Theatre Workshop</h3><p>Lorem ipsum dolor sit amet consectetur elit phasellus molestie id commodo velit fermo</p></div>
			}
		},
		{
			name: "Item Hub Revised",
			props: {
				modifiers: ['hub', 'hubRevised'],
				media : [{ url: "/ui/img/placeholder/placeholder.png", "fluid": false }],
				fluid : false,
				linkClass: "Link  Link--underline",
				linkText: "More",
				content: <div><h3>Youth Theatre Workshop</h3><p>Lorem ipsum dolor sit amet consectetur elit phasellus molestie id commodo velit fermo</p></div>
			}
		},
		{
			name: "Item Hub Multi Landscape",
			props: {
				modifiers: ['multiHub', 'landscape'],
				media : [{ url: "/ui/img/placeholder/placeholder4.png", "fluid": false }, { url:"/ui/img/placeholder/placeholder5.png", "fluid": false }],
				linkClass: "Link  Link--underline",
				linkText: "More",
			 content: <div><h2>Under 16? Get free entry</h2><p>Lorem ipsum dolor sit amet consectetur elit phasellus molestie id commodo velit fermo</p></div>
			}
		},

	]
}

