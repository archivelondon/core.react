import React, {Component} from 'react'

let IDLE = 0;

class Idle extends Component {

  constructor() {
    super()
    this.state={idle:false};
  }

  actions = 0;
  timer = 0;

  componentDidMount = () => {

      this.startTimer();
      this.startListeners();
  }

  componentWillUnmount = () => {

      if(this.timer){
        clearTimeout(this.timer);
      }
      this.clearListeners();
  }

  startTimer = () => {

      if(this.timer){
        clearTimeout(this.timer);
      }
      this.actions = 0;
      

      this.timer = setInterval(this.isIdle,this.props.timeout || 5000);

      // Set back to idle unless the once
      if(!this.props.once){
        if(this.state.idle) this.setState({idle:false});
      }
  }
  action = () => {
    if(!this.state.idle) this.actions++;
    if(this.props.resetAtAction) this.reset();
  }
  reset = () => {

      this.startTimer();

      if(this.state.idle) this.setState({idle:false});
      
  }

  startListeners = () => {
    document.addEventListener('click',this.reset);
    document.addEventListener('mousemove',this.action);
    document.addEventListener('touchmove',this.action);
    document.addEventListener("mousewheel", this.action, false);
    // nonstandard: Firefox
    document.addEventListener("DOMMouseScroll", this.action, false);
  }

  clearListeners = () => {
    document.removeEventListener('click',this.reset);
    document.removeEventListener('mousemove',this.action);
    document.removeEventListener('touchmove',this.action);
    document.removeEventListener("mousewheel", this.action, false);
    // nonstandard: Firefox
    document.removeEventListener("DOMMouseScroll", this.action, false);
  }

  isIdle = () => {

    if(this.actions){
      this.startTimer();
      return false;
    }
    if(!this.state.idle) this.setState({idle:true});
    
  }

  render () {
      return this.props.render({idle:this.state.idle});
      
  }
}

export default Idle
