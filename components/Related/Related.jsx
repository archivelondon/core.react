import React, {Component} from 'react'
import Base from '../BaseComponent'
import Grid from '../Grid'
import Content from '../Content'
import Item from '../Item'
import Media from '../Media'

import Each from '../../utils/Each'

import {
  Link
} from 'react-router-dom';


import Entry from '../../models/Entry'



class Related extends Component {





  render () {

    let {related, relatedTitle} = this.props.to

    let modifier = this.props.modifier ? this.props.modifier : ''

    const subtitle = (item) => {
        switch(item.template) {
            case 'location':
                return location(item)
                break
            case 'article':
                return article(item)
                break

            case 'event':
                return event(item)
                break
            default:
                return null
                break
        }
    }

    const location = (item) => {

        return (
            <div className="Item-subTitle">
                <Content>
                    <h6>{item.locationType}</h6>
                </Content>
            </div>
            )
        
    } 
    const article = (item) => {

        return (
            <div className="Item-subTitle">
                <Content>
                    <h5>{item.newsCategory}</h5>
                </Content>
            </div>
            )
        
    } 

    const event = (item) => {
        
         return (
            <div className="Item-subTitle">
                <Content>
                    <h5>{item.eventCategory}</h5>
                </Content>
            </div>
            )

    } 



    if(!related || !related.length) {
        return null
    }

    return (
              <div className="Template-section Template-section--narrow Template-section-container">

                                <div className="Template-heading">
                                    <Content modifier={'alt'} html={"<h1>"+relatedTitle+"</h1>"} />
                                </div>
                                <div className="Template-content">

                                <Grid modifiers={['2',modifier]} >

                                        <Entry id={related} results={
                                                (items) => (

                                                        <Each items={items} render={ (item) => (
                                                                <div className="Grid-item">

                                                                    <Item modifiers={['grid']}>
                                                                        <Link to={item.uri}>
                                                                            <div className="Item-media">
                                                                                <Media id={item.image[0]} />
                                                                            </div>
                                                                            <div className="Item-head">
                                                                        <div className="Item-title">
                                                                            <Content
                                                                                modifier={
                                                                                    "yellow"
                                                                                }
                                                                                html={'<h5>'+item.title+'</h5>'}
                                                                            >
                                                                            </Content>
                                                                        </div>
                                                                        {subtitle(item)}
                                                                        
                                                                    </div>
                                                                        </Link>
                                                                    </Item>


                                                                </div>
                                                        )}/>
                                                )
                                        }/>


                                        </Grid>

                                </div>

                            </div>


    )



  }
}

export default Related
