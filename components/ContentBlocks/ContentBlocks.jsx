import React, {Component} from 'react'


import Media from '../Media'
import Content from '../Content'

import Each from '../../utils/Each'

import Asset from '../../models/Asset'
import Entry from '../../models/Entry'

const index = 0;

class ContentBlocks extends Component {

  // let first = true;

	block = (item,ix) => {

		let aside,content,classname;

    classname = (ix == 0) ? "Template-section Template-section--narrow  Template-section--first" : "Template-section Template-section--narrow";


		switch(item.type){

			case 'text':
				aside = item.blockTitle;
				content = (<Content modifier={'text'} html={item.text} ></Content>);


			break;
			case 'image':
				aside = item.blockTitle;
				content = (<Media fluid={false} id={item.image[0]} />);
			break;
			case 'pullQuote':
				aside = item.blockTitle;
				content = (<Content><pullquote>{item.text}</pullquote></Content>);
			break;


		}

		if(this.props.modifier && this.props.modifier == 'about' && item.type == 'image') {
			return (
				<div className={classname + ' Template-section--image'}>
					<div className="Template-heading">
					</div>
					<div className="Template-content">
						{content}
						<Content html={'<h4>'+aside+'</h4>'}/>
					</div>
				</div>
			);
		} else {
			return (
				<div className={classname}>
					<div className="Template-heading">
						<Content>
							<h1>{aside}</h1>
						</Content>
					</div>
					<div className="Template-content">
						{content}
					</div>
				</div>
			);
		}
		


	}

  render () {

  	if(!this.props.blocks) return null;

    return (


        <div className="Template-sections">
        	<Each items={this.props.blocks} render={ (item,ix) => (

        		this.block(item, ix)

        	)} />


		</div>

    )
  }
}

export default ContentBlocks;
