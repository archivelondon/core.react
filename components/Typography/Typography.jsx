import React, {Component} from 'react'

class Text extends Component {

  render () {

    const {
      title
    } = this.props

    return (
      <div>
        <div className={this.props.className}>{title}</div>
      </div>
    )
  }
}

export default Text
