import React, {Component} from 'react'

import _coreStories from '../../../_coreStories.js';

const textUtils = _coreStories.textUtils

const arr = [];

textUtils.map((textUtil,ix) => { 

  if(_coreStories.excludeIcons.indexOf(textUtil) == -1) {
    arr.push({
      name: textUtil,
      props: {
        key: ix,
        title: "Hello world",
        className: "Text Text--"+textUtil
      }
    })
  }
});


export default {
    title: "Typography",
    stories: arr
}

