Text Utils :

Sizes —
%u-text--mirco ~ 14px
%u-text--xSmall ~ 16px
%u-text--small ~ 20/20px
%u-text--normal ~ 24px ($base-font-size)
%u-text--medium ~ 32/32px
%u-text--large ~ 48px
%u-text--xLarge ~ 56/56px
%u-text--xxLarge ~ 64/64px

Colours —
%u-text--yellow

Fonts — 
%u-text--sabon ($base-font-family)
%u-text--fenway ($base-font-family-sans)



