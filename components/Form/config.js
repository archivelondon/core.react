import React, {Component} from 'react'

export default {
    title: "Form",
    stories: [
      {
      name: "search",
      props: {
        modifier: 'search',
        fields: [{'name':'search', type:'text', placeholder:"Search & hit enter"}]
      }
    },{
      name: "Sign up",
      props: {
        modifier: 'signUp',
        heading: 'Join our mailing list',
        fields: [{'name':'name', type:'text', placeholder:"Name*"},{'name':'email', type:'text', placeholder:"Email*"},{'name':'postcode', type:'text', placeholder:"Postcode"}],
        submit: "Subscribe"
      }
    }
    ],
}
