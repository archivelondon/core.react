import React, {Component} from 'react'

import _coreStories from '../../../_coreStories.js';

const icons = [];

const req = require.context('../../../../../public/ui/img/icons', false, /\.svg$/);
req.keys().forEach(filename => {
	let icon = filename.replace('./','').replace('.svg', '');
	icons.push(icon);
});



const iconsObj = [];

icons.map((icon,ix) => { 

	if(_coreStories.excludeIcons.indexOf(icon) == -1) {
		iconsObj.push({
			name: icon,
			props: {
				key: ix,
				type: [icon]
			}
		})
	}
});

icons.map((icon,ix) => { 

	if(_coreStories.excludeIcons.indexOf(icon) == -1) {
		iconsObj.push({
			name: 'Glyph '+icon,
			props: {
				key: ix,
				type: [icon],
				glyph:true
			}
		})
	}
});

console.log('iconsObj',iconsObj);

export default {
    title: "Icon",
    stories: iconsObj
}

