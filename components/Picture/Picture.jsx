import React, { Component } from 'react';
import Resize from '../../utils/Resize';

let pictureStyle = {
  display:'block'
}

class Picture extends Component {

  el=null;
  img=null;
  
	constructor(props) {
    super(props);
    this.state = {width:0,loaded:0};
  }

  componentDidMount() { 
      this.resize();
	}


  setRef = (el) => {
      this.el = el;
  }

  setImage = (el) => {
      this.img = el;
  }

  loaded = () => {
    console.log('loaded ',this.img.src);
  }

  resize = () => {

      let w = 0;
      if(this.el){
        let w = this.el.offsetWidth;
        if(w != this.state.width){
          console.log('w',w,this.state.width);
          this.setState({width:w});
        }
      }

  }
  render() {

      let smallest = 0;
      let smallestSrc = 0;

      let srcSet = [];

      this.props.images.forEach((i,ix) => {
        if(!smallest || i.width < smallest){
          smallest = i.width;
          smallestSrc = i.src;
        }

        srcSet.push(`${i.src} ${i.width}w`);
      });

      return (
        <Resize onResize={this.resize}>
            
                
                {(()=>{

                  if(this.el && this.state.width){
                    return (
                      <picture className="Picture" ref={this.setRef} data-width={this.state.width} style={pictureStyle}>
                      <source srcSet={srcSet.join(',')} sizes={`${this.state.width}px`} />
                      <img ref={this.setImage} src={smallestSrc} alt={`${this.props.alt || ''}`}  />

                      {(()=>{
                        if(this.props.caption){
                          return <p>{this.props.caption}</p>
                        }
                      })()}
                      </picture>
                    )
                  }else{

                    return (
                      <picture className="Picture" ref={this.setRef} style={pictureStyle} />
                    )
                  }

                })()}

        </Resize>
      )
	}
}

export default Picture;
