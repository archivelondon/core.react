import React, {Component} from 'react'
import {TimelineMax,TweenLite,Linear,Sine,Expo,Power0,Power4} from 'gsap';
import {
  withRouter
} from 'react-router-dom';

import {withStore} from '../../utils/Store';

/*

TransitionEntry - cross fade one entry with another when the route changes

*/

class TransitionEntry extends Component {

  animating=0;

  constructor(props) {

      super(props);
      this.state = {entry:{},prevEntry:{}}

  }

  componentDidMount() {

   
        
        this.fade();
   
      

  }

 componentWillReceiveProps(nextProps) {
   
      const currentPage = this.props.location.pathname;
      const nextPage = nextProps.location.pathname;

      if (currentPage !== nextPage) {
        this.fade();
      }
    }

  fade = (callback) => {
      const _this = this;

      //if(_this.animating) return;

      _this.animating = 1;

      let duration = this.props.duration || 0.8;
      if(this.props.duration === 0) duration = 0;
      
      TweenLite.to(_this.refs.el,duration,{opacity:0,ease:Expo.easeOut,onComplete:function(){

            _this.swapEntry();

           TweenLite.to(_this.refs.el,duration,{opacity:1,ease:Sine.easeOut,onComplete:function(){

            _this.animating = 0;

          }});

      }});

  }

  swapEntry = () => {

      let {entries} = this.props.data;
      let {location} = this.props;

      

      const match = (entries.find((i) => {
        return (i.uri == location.pathname)
      }) )

      const found = typeof match != 'undefined';
      let entry = {};
      if(found){

        entry = match;

      }

      this.setState({

          entry:entry,
          prevEntry:this.state.entry

      })

  }

  render () {

    const {entries} = this.props.data;
    if(!entries) return null;


    return (

      <div ref="el">

        { (!Object.keys(this.state.entry).length) ? null : this.props.children(this.state.entry) }

      </div>
    )
  }
}

export default withRouter(withStore(TransitionEntry));