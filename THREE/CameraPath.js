import * as THREE from 'three';
import SETTINGS from '../SETTINGS';


export default function CameraPath(){

  let ARC_SEGMENTS = 200;
  let oriVectors = [new THREE.Vector3(941.1276737959345, 452.51481137238443, -131.04455907241208),
  new THREE.Vector3(283.8021944897115, 278.6928790271119, 273.05090978449033),
  new THREE.Vector3(-45.506878551168754, 176.4306956436485, -878.3636775607574),
  new THREE.Vector3(-437.0425417241941, 491.1365363371675, 85.3380712895254),
  new THREE.Vector3(-1406.2853556187924, -0.1537975165667831, -342.1784816454348)];

  let vectors = SETTINGS.COORDS;



const curve = new THREE.CatmullRomCurve3( vectors );
        curve.curveType = 'centripetal';
var points = curve.getPoints( 200 );
var geometry = new THREE.Geometry();

points.forEach((point,ix) => {

 
    geometry.vertices.push(point);

})

let lineMat = new THREE.LineBasicMaterial( {
          color: 0x00ff00,
          transparent:true,
          opacity: 0,
          linewidth: 0
          } )

curve.mesh = new THREE.Line( geometry.clone(),lineMat );
        curve.mesh.castShadow = false;



    return {object:curve.mesh,curve:curve};
}