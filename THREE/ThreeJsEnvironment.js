import React, { Component } from 'react';

import * as THREE from 'three';

import NormalizedScroller from '../components/NormalizedScroller';
import MousePos from '../components/MousePos';

// Three
import ThreeJs from './ThreeJs';

// ThreeJS Helpers
import ImagePlane from './ImagePlane';
import World from './World';

import SVGLoader from './SVGLoader';

//import {ObjectLoader} from 'three/src/loaders/ObjectLoader';

const SubdivisionModifier = require('three-subdivision-modifier');

// Install OBJLoader into THREE
var OBJLoader = require('three-obj-loader');
OBJLoader(THREE);


class ThreeJsEnvironment extends Component {

	// Core THREE elements
	camera;scene;renderer;play;pause;renderScene;controls;

	// Touch device
	touch = (window.DeviceOrientationEvent && 'ontouchstart' in window);

	// Array of items passed in to the environment
	items = [];

	// Store of the scene objects
	sceneObjects = [];

	// Canonical keys representing the direct scene added items, their data and their hitarea in a single lookup table
	lookup = {
		/*

			'8DF56734-UI-223434FC' : {
				data : {} // Item data from JSON
				object : {} // Object attached to the scene
				hitarea : {} // Object used for hit tests
			}

		*/
	}

	// Store of raycastable objects to determine a hit on mouse over or click
	hitareas = [];

	// Objects - arbitrary data passed in to the component
	objects = {};

	// Helpers to pass up to the parent (3D object contstructors)
	helpers = {
		THREE,
		SVGLoader,
		SubdivisionModifier,
		ImagePlane,
		World,
		easeTo : (a,b,f=0.2) => {

  			return (a * (1.0 - f)) + (b * f);

  		},
		absolutePosition : (object) => {

			// Return the world position for a nested object
			object.updateMatrixWorld();
			let vector = new THREE.Vector3();
		    
			vector.setFromMatrixPosition( object.matrixWorld );
			

			return vector;

		},
		toScreenXY: function ( pos, camera ) {

			// Return the X,Y in pixels of a given vector, relative to the camera viewport and the browser window
			camera.updateMatrixWorld(); 
			
		    
		    var v = pos.clone();
    			v.project(camera);
		     var percX = (v.x + 1) / 2;
            var percY = (-v.y + 1) / 2;
            var left = percX * window.innerWidth; // TODO - relative to div size
            var top = percY * window.innerHeight; // TODO - relative to div size

            return new THREE.Vector2(left, top);
		  
		    

		},
		reset : () => {
			this.items.forEach((i)=>{
				i.object.position.set(0,0,0);
				i.object.scale.set(1,1,1);
			})
		},
		fade : (object,duration,opts) => {

			if(!object) return;

			if(object.material){
				window.TweenLite.to(object.material,duration,opts);
			}else{
				if(object.children && object.children.length){
					object.children.forEach(c=>{this.helpers.fade(c,duration,opts)});
				}
			}


		}
	}

	// Custom storage for key/value pairs
	storedItems = {};
	// Simple setter and getter for the store
	store = {
		add : (label,item) => {
			this.storedItems[label] = item;
		},
		get : (label) => {
			return this.storedItems[label];
		}
	}

	// Mouse position
	mouse = {x:0,y:0};

	// Once-created raycaster and vector containers
	raycaster = new THREE.Raycaster(); // create once
	mousevector = new THREE.Vector2();
	vector = new THREE.Vector3();

	quickGUID = () => {
    	return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	}

	triggers = {};

	constructor(props) {
		super(props);
    	this.state = {scrollDelta:0,scrollUpdates:0};
	}

	componentDidMount() {

	}

	componentWillUnmount() {

	}

	componentWillReceiveProps = (props) => {


		// e.g:
		if(!props.triggers) return;

		let _this = this;

		props.triggers.forEach((t)=>{

			if(!_this.triggers.hasOwnProperty(t.type)){
				this.callTrigger(t);
			}else if((t.key != _this.triggers[t.type].key)){
				this.callTrigger(t);
			}

		})
		

	}

	shouldComponentUpdate = () => {
		return false;
	}

	callTrigger = (opts) => {

		// Set the trigger
		this.triggers[opts.type] = opts;

		// Run the callback
		opts.callback(opts,this._getState());

	}

	_getState = () => {
		return {render:this.renderScene,play:this.play,pause:this.pause,mouse:this.mouse,touch:this.touch,lookup:this.lookup,camera:this.camera,scene:this.scene,renderer:this.renderer,controls:this.controls,helpers:this.helpers,store: this.store,sceneObjects:this.sceneObjects,objects:this.objects,items:this.items,props:this.props};
	}

	_init = ({scene,camera,renderer,play,pause,render,controls}) => {
		// When THREE scene is set up...
		let _this = this;
		
		// Objects passed in from THREEJS component
		this.scene = scene;
		this.camera = camera;
		this.renderer = renderer;
		this.play = play;
		this.pause = pause;
		this.renderScene = render;
		this.controls = controls;

		let items = [];

		// Set From props
		if(this.props.items) items = this.props.items;
		if(this.props.objects) this.objects = this.props.objects;
		if(this.props.helpers) this.helpers = Object.assign(this.helpers,this.props.helpers);

		// Init hook for parent
		if(this.props.hasOwnProperty('onInit')){
			let initObjects = this.props.onInit(this._getState());
		}

		// Loop through the items
		_this._createItems(items,scene);
		
		// Finished creating items
		if(this.props.hasOwnProperty('onCreatedItems')){
			this.props.onCreatedItems(this._getState());
		}

		// Resize Listener
		if(this.props.hasOwnProperty('onResize')){

			window.addEventListener('resize',function(){
				_this.props.onResize(_this._getState());
			});
			
		}
		
	}

	_createItems = (items,parent) => {

		let _this = this;

		items.forEach((i)=>{

			// Create an id if not already
			if(!i.id) i.id = this.quickGUID();

			// Call the addItem hook
			let added = _this._addItem(i);
			let item = {...i,...added};
			
			// if hook returned something...
			if(item){
				// Add to the scene
				parent.add(item.object);

				item.object.data = i; // Store the item data in the object
				item.data = i; // Store the item data alongside the object
				_this.sceneObjects.push(item.object); // Push onto the sceneObjects array for keeping track

				// Check if returned item has a hitarea property. If not, use the object as the hitarea
				if(!item.hasOwnProperty('hitarea')){
					_this.hitareas.push({
						data: i,
						object: item.object
					});
				}else{
					// Allow {hitarea:false} to ignore from hit tests
					if(item.hitarea){
						_this.hitareas.push({
							data: i,
							object: item.hitarea
						});
					}
				}

				// Check for children
				if(i.hasOwnProperty('children')){

					// Recurse into children, sending this in as the parent
					_this._createItems(i.children,item.object);

				}
				
				// Hook for a callback after the item has been added
				if(i.hasOwnProperty('addedItem')){
					i.addedItem(item,this._getState());
				}

				// Component hook for addedItem
				if(_this.props.hasOwnProperty('addedItem')){
					_this.props.addedItem(item,this._getState());
				}

				// If the item has an id, store it
				if(i.id){
					_this.store.add(i.id,item);
				}

				// Store the item, now fully created
				_this.items.push(item);

				
			}


		});

		

		

	}

	_loop = () => {
		let state = this._getState();
		let _this = this;
		// Render loop
		if(this.props.onLoop){
			this.props.onLoop(state);
		}

		// Check individual items for onLoop
		this.items.forEach((i)=>{
			if(i.onLoop){
				let obj = _this.store.get(i.id);
				i.onLoop.call(obj,state,obj);
			}
		});

	}

	_addItem = (item) => {

		// Per item onAddItem or create function
		if(item.hasOwnProperty('onAddItem')){
			return item.onAddItem(item,this._getState());
		}else if(item.hasOwnProperty('create')){
			return item.create(item,this._getState());
		}

		// Generic per component onAddItem function
		if(this.props.onAddItem){
			return this.props.onAddItem(item,this._getState());
		}

	}

	_scroll = (data) => {

		if(this.props.onScroll){
			this.props.onScroll(data,this._getState());
		}

	}

	_mouseOver = null;

	_mouseMove = (mousedata) => {

	
		this.mouse = mousedata;
		let state = this._getState();

		if(this.props.onMouseMove && state.scene){
			this.props.onMouseMove(mousedata,state);
		}

		let mouseOver = this._getMouseTarget();
		let bubbleOff = true;
		let bubbleOn = true;

		if(mouseOver != this._mouseOver){

			

			// Rolled off the last object
			if(this._mouseOver){

				if(this._mouseOver.onHoverOff){
					bubbleOff = this._mouseOver.onHoverOff.call( this._mouseOver ,this._getState(), this._mouseOver)
				}
				if(this.props.onHoverOff && (bubbleOff !== false)){
					this.props.onHoverOff( {target: this._mouseOver} ,this._getState());
				}

			}

			// Rolled over a new object
			if(mouseOver){

				if(mouseOver.onHover){
					bubbleOn = mouseOver.onHover.call( mouseOver ,this._getState(), mouseOver)
				}
				if(this.props.onHover && (bubbleOn !== false)){
					this.props.onHover( {target:mouseOver} ,this._getState());
				}

			}

			this._mouseOver = mouseOver;
		}

	}

	_getMouseTarget = () => {

		// Loop through all hitareas to see if we clicked a target
		let target = null;
		let _this = this;

		let state = this._getState();

		let {mouse,renderer,camera,mousevector,vector,raycaster,hitareas} = this;

		if((mouse.x > 0) && (mouse.y > 0)){

	
			// create a Ray with origin at the mouse position
			//   and direction into the scene (camera direction)
			
			mousevector.x = ( mouse.x / renderer.domElement.clientWidth ) * 2 - 1;
			mousevector.y = - ( mouse.y / renderer.domElement.clientHeight ) * 2 + 1;

			vector.set( ( mouse.x / renderer.domElement.clientWidth ) * 2 - 1, - ( mouse.y /renderer.domElement.clientHeight ) * 2 + 1, 0.5 ); // z = 0.5 important!

    		vector.unproject( camera );


			raycaster.setFromCamera( mousevector, camera );

			var intersectable = hitareas.map((i)=>(i.object));

			var intersects = raycaster.intersectObjects(  intersectable );


			if(intersects.length){
				let hit = intersects[0].object;

				if(hit.hasOwnProperty('data')){
					target = _this.store.get(hit.data.id);
				}else{
					if(hit.parent && hit.parent.hasOwnProperty('data')){
						target = _this.store.get(hit.parent.data.id);
					}
				}
			}


		}	

		return target;
	}

	_click = (data) => {

		
		
		let target = this._getMouseTarget();
		let bubble = true;

		if(target && target.onClick){
			bubble = target.onClick.call(target,this._getState(),target);
		}

		if(this.props.onClick && (bubble !== false)){
			this.props.onClick({target:target},this._getState());
		}

	}


	/*

		RENDER

	*/

	render() {

    	let _this = this;
    	let objects = (this.props.objects) ? this.props.objects : {};
    	let useWindow = (this.props.hasOwnProperty('useWindow')) ? this.props.useWindow : true;
    	let useTilt = (this.props.hasOwnProperty('useTilt')) ? this.props.useTilt : false;
    	let mouseEase = (this.props.hasOwnProperty('mouseEase')) ? this.props.mouseEase : false;
    	let customRender = (this.props.hasOwnProperty('render')) ? this.props.render : null;
    	
			return (
			
	          <NormalizedScroller shouldMove={
	          	() => {

	          		if(_this.props.shouldScroll) return _this.props.shouldScroll(_this._getState());
	          		return true;
	          	}
	          } manualUpdate onUpdate={(data)=>{
	          		// Update scroll delta
	              _this._scroll(data);
	          }} getHeight={()=>{
	            return window.innerHeight * 4;
	          }}>
	            <MousePos useTilt={useTilt} useWindow={useWindow} ease={mouseEase}>
	              {(mousedata) => {
	             		
	              		// Update mouse pos
	             		_this._mouseMove(mousedata);

	                  return (<div onClick={this._click}><ThreeJs onInit={this._init} objects={objects} noControls onLoop={this._loop} render={customRender} /></div>)
	                  
	     
	              }}
	            </MousePos>
	          </NormalizedScroller>
			);

	}
}

export default ThreeJsEnvironment;
