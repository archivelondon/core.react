import * as THREE from 'three';

export default function Particles(num=20000){

  let geometry = new THREE.Geometry();


    [...Array(num)].forEach((o,ix) =>{
      var vertex = new THREE.Vector3();
      vertex.x = Math.random() * 2000 - 1000;
      vertex.y = Math.random() * 2000 - 1000;
      vertex.z = Math.random() * 2000 - 1000;
      geometry.vertices.push( vertex );
    });

    let parameters = [
      [ [1, 1, 0.5], 5 ],
      [ [0.95, 1, 0.5], 4 ],
      [ [0.90, 1, 0.5], 3 ],
      [ [0.85, 1, 0.5], 2 ],
      [ [0.80, 1, 0.5], 1 ]
    ];


      let group = new THREE.Group();
      let material = new THREE.PointsMaterial( { color: 0xFFFFFF,size: 1 } );
      let particles = new THREE.Points( geometry, material );
      //particles.rotation.x = Math.random() * 6;
      //particles.rotation.y = Math.random() * 6;
      //particles.rotation.z = Math.random() * 6;

      particles.rotation.z = Math.PI / 2;
      particles.rotation.x = Math.PI / 2;
      particles.rotation.y = Math.PI / 2;

      let particles2 = new THREE.Points( geometry, material );
      particles.scale.x =2;
      particles.scale.y =2;
      particles.scale.z =2;
      group.add(particles);
      group.add(particles2);

      const onRender = function(){

        particles.rotation.y += 0.0005;
        particles2.rotation.z += 0.0003;

      }

    return {object:group,onRender};
}
