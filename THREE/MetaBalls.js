import React, { Component } from 'react';
import * as THREE from 'three';
import MarchingCubes from './MarchingCubes';


export default function MetaBalls(){

    // MARCHING CUBES
			const resolution = 60;
			const numBlobs = 10;
			const material = new THREE.MeshLambertMaterial({ color: 0xFF89FF } );

			const effect = new MarchingCubes( resolution, material, true, true );
			effect.position.set( 0, 0, 0 );
			effect.scale.set( 1000, 1000, 1000 );
			effect.enableUvs = false;
			effect.enableColors = false;
			

			function updateCubes( object, time, numblobs, floor, wallx, wallz ) {
			object.reset();
			// fill the field with some metaballs
			var i, ballx, bally, ballz, subtract, strength;
			subtract = 12;
			strength = 1.2 / ( ( Math.sqrt( numblobs ) - 1 ) / 4 + 1 );
			for ( i = 0; i < numblobs; i ++ ) {
				ballx = Math.sin( i + 1.26 * time * ( 1.03 + 0.5 * Math.cos( 0.21 * i ) ) ) * 0.27 + 0.5;
				bally = Math.abs( Math.cos( i + 1.12 * time * Math.cos( 1.22 + 0.1424 * i ) ) ) * 0.77; // dip into the floor
				ballz = Math.cos( i + 1.32 * time * 0.1 * Math.sin( ( 0.92 + 0.53 * i ) ) ) * 0.27 + 0.5;
				object.addBall(ballx, bally, ballz, strength, subtract);
			}
			if ( floor ) object.addPlaneY( 2, 12 );
			if ( wallz ) object.addPlaneZ( 2, 12 );
			if ( wallx ) object.addPlaneX( 2, 12 );
		}

		const clock = new THREE.Clock(true);

		const onRender = function (){

			updateCubes( effect, clock.getElapsedTime()*0.1, numBlobs, false, false, false );

		}

		


    
    return {object:effect,onRender};

}

