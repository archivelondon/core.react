import * as THREE from 'three';

// Return a THREEjs geometry of ubes or spheres of a given number

export default function Geometry(objectType, numObjects) {

  var geometry = new THREE.Geometry();
  
  function applyVertexColors( g, c ) {

    g.faces.forEach( function( f ) {

      var n = ( f instanceof THREE.Face3 ) ? 3 : 4;

      for( var j = 0; j < n; j ++ ) {

        f.vertexColors[ j ] = c;

      }

    } );

  }

  for ( var i = 0; i < numObjects; i ++ ) {
  
    var position = new THREE.Vector3();

    position.x = Math.random() * 10000 - 5000;
    position.y = Math.random() * 6000 - 3000;
    position.z = Math.random() * 8000 - 4000;

    var rotation = new THREE.Euler();

    rotation.x = Math.random() * 2 * Math.PI;
    rotation.y = Math.random() * 2 * Math.PI;
    rotation.z = Math.random() * 2 * Math.PI;

    var scale = new THREE.Vector3();

    var geom, color=new THREE.Color();

    scale.x = Math.random() * 200 + 100;

    if ( objectType == "cube" )
    {
      geom = new THREE.CubeGeometry( 1, 1, 1 );
      scale.y = Math.random() * 200 + 100;
      scale.z = Math.random() * 200 + 100;
      color.setRGB( 0, 0, Math.random()+0.1 );
    }
    else if ( objectType == "sphere" )
    {
      geom = new THREE.IcosahedronGeometry( 1, 1 )
      scale.y = scale.z = scale.x;
      color.setRGB( Math.random()+0.1, 0, 0 );
    }
    
    // give the geom's vertices a random color, to be displayed
    applyVertexColors( geom, color );

    var cube = new THREE.Mesh( geom );
    cube.position.copy( position );
    cube.rotation.copy( rotation );
    cube.scale.copy( scale );

    THREE.GeometryUtils.merge( geometry, cube );

  }

  return geometry;

}