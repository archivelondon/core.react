import * as THREE from 'three';
import ImagePlane from './ImagePlane';


export default function World(data){

  let sphere;
  var group = new THREE.Group();

    let color = (data && data.color) ? data.color : 0xFFFFFF;
    var geometry2 = new THREE.SphereGeometry( 400, 6, 6 );
    var material2 = new THREE.MeshBasicMaterial( { color: color, wireframe: true } );
    sphere = new THREE.Mesh( geometry2, material2 );

    group.add( sphere );

    if(data && data.content){
    data.content.map((obj) => {
      const imagePlane = ImagePlane(obj);
      group.add(imagePlane.object)
    })
    }


    return {object:group,sphere};
}
