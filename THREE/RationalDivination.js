import * as THREE from 'three';
import World from './World';

export default function NoiseFloor(data){

  let world = World(data);

  return {object:world.object,sphere:world.sphere};
}
