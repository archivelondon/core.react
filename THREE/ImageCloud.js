import * as THREE from 'three';
import {TweenLite,TimelineLite,Sine,Expo,Power1,Power0} from 'gsap';

export default function ImageCloud(imageUrl){

  let imageCloud = new THREE.Group();
  let particles = new THREE.Points();
  let tween;
  let expanded = true;

  const getImageData = function(image) {

    const canvas = document.createElement("canvas");

    canvas.width = image.width;
    canvas.height = image.height;

    const ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, 0);

    return ctx.getImageData(0,0, image.width, image.height);
  }

  const loader = new THREE.TextureLoader();

  

    const drawDataImage =  function(imageData) {

      const geometry = new THREE.Geometry();
      const density = 1;

      for (let y = 0, y2 = (imageData.height * density); y < y2; y += 2) {
        for (let x = 0, x2 = (imageData.width * density); x < x2; x += 2) {
          if (imageData.data[(x * 4 + y * 4 * (imageData.width * density)) + 3] > 128) {

            let r = imageData.data[(((imageData.width * density) * y) + x) * 4];
            let g = imageData.data[(((imageData.width * density) * y) + x) * 4 + 1];
            let b = imageData.data[(((imageData.width * density) * y) + x) * 4 + 2];

            let vertex = new THREE.Vector3();
            vertex.x = Math.random() * 2000 - 500;
            vertex.y = Math.random() * 2000 - 500;
            vertex.z = -Math.random() * 2000;

            vertex.startPoint = {
              x: vertex.x,
              y: vertex.y,
              z: vertex.z
            }

            vertex.destination = {
              x: x - imageData.width / 2,
              y: -y + imageData.height / 2,
              z: 0
            };

            vertex.speed = Math.random() / 2000 + 0.015;
            geometry.vertices.push(vertex);

            geometry.colors.push(new THREE.Color(r/255,g/255,b/255));


          }
        }
      }

      const material = new THREE.PointCloudMaterial({size:3,vertexColors:THREE.VertexColors});

      // setColor(geometry);

      particles = new THREE.PointCloud(geometry, material);
      // Remove any children...
      for (var i = imageCloud.children.length - 1; i >= 0; i--) {
          imageCloud.remove(imageCloud.children[i]);
      }
      imageCloud.add(particles);

      return imageCloud;
    }

    const clear = function(){

        for (var i = imageCloud.children.length - 1; i >= 0; i--) {
          imageCloud.remove(imageCloud.children[i]);
      }

    }
    const setColor = function(geometry) {

      for(let i = 0; i < geometry.vertices.length; i++ ) {

        geometry.colors[i] = new THREE.Color(1,Math.random(),0);

        // geometry.colors[i].setRGB(1,0,0)


      }

      geometry.verticesNeedUpdate = true;

    }

    const buildImage = function() {

      if( particles.geometry.vertices ) {
        let progress = {value: 0};
        expanded = false;
        if (tween) tween.kill();

        tween = TweenLite.to(progress, 5, {value: 1, ease:Sine.easeOut, onUpdate: function() {

            for (var i = 0, j = particles.geometry.vertices.length; i < j; i++) {

              const particle = particles.geometry.vertices[i];
              const ease = (progress.value/3);
              particle.x += (particle.destination.x - particle.x) * ease;
              particle.y += (particle.destination.y - particle.y) * ease;
              particle.z += (particle.destination.z - particle.z) * ease;
            }

            particles.geometry.verticesNeedUpdate = true;
          }
         }
         );
      }
    }

    const showImage = function(src) {

      loader.load(src,

    function( texture ) {
        let imageData = getImageData(texture.image);


        if(expanded){
            drawDataImage(imageData);
            buildImage();
        }else{


            spreadImage(function(){

                drawDataImage(imageData);
                buildImage();

            })

        }

    });
    }

    const spreadImage = function(callback) {

      if( particles.geometry.vertices ) {
        let progress = {value: 0};
        expanded = true;

        if (tween) tween.kill();

        tween = TweenLite.to(progress, 3, {value: 1, ease:Expo.easeOut, onUpdate: function() {

            for (var i = 0, j = particles.geometry.vertices.length; i < j; i++) {

              const particle = particles.geometry.vertices[i];
              const ease = (progress.value/3);

              particle.x += (particle.startPoint.x - particle.x) * ease;
              particle.y += (particle.startPoint.y - particle.y) * ease;
              particle.z += (particle.startPoint.z - particle.z) * ease;

            }

            particles.geometry.verticesNeedUpdate = true;

          },
          onComplete : function(){

            if(callback) callback();

         }

         }
         );
      }

    }



    const onRender = function() {}


    return {object:imageCloud, onRender, buildImage, spreadImage, showImage, clear};

}
