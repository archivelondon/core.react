import * as THREE from 'three';

var TERRAINGEN =
{
	/// HELPERS METHODS ///

	/** 
	 * Create a DOM canvas element
	 * @param inWidth Width of the canvas
	 * @param inHeight Height of the canvas
	 * @return The created canvas
	 */
	CreateCanvas: function( inWidth, inHeight ) 
	{
		var canvas = document.createElement( "canvas" );
		canvas.width = inWidth;
		canvas.height = inHeight;

		return canvas;
	},
	
	/**
	 * Create vertices of the terrain from the given canvas and parameters
	 * @param inNoise 2D Canvas that store height informations
	 * @param inGeometry Geometry where fill vertices
	 * @param inDepth Depth of the terrain
	 * @param inWidth Width of the terrain
	 * @param inHeight Height of the terrain
	 */
	CreateVertices: function( inNoise, inGeometry, inDepth, inWidth, inHeight )
	{
		var positions = inGeometry.getAttribute( 'position' ).array;
		var context = inNoise.getContext('2d'),
			imgData = context.getImageData( 0, 0, inNoise.width, inNoise.height ),
			pixels = imgData.data,
			scaleX = inWidth / ( inNoise.width - 1 ),
			scaleY = inDepth / 255,
			scaleZ= inHeight / ( inNoise.height - 1 ),
			id = 0,
			pixel = 0,
			offsetX = - inNoise.width / 2,
			offsetZ = - inNoise.height / 2;
		
		//for( var y = inNoise.height-1; y >= 0; --y )
		for( var y = 0; y < inNoise.height; ++y )
		{
			for( var x = 0; x < inNoise.width; ++x )
			{
				//inGeometry.vertices.push( new THREE.Vector3( scaleX * ( x + offsetX ), scaleY * ( pixels[id * 4 + 1] ), scaleZ * ( y + offsetZ ) ) );
				positions[id ++] = scaleX * ( x + offsetX );
				positions[id ++] = scaleY * ( pixels[ (pixel ++) * 4 + 1] );
				positions[id ++] = scaleZ * ( y + offsetZ );
			}
		}


	},
	
	/**
	 * Create faces of the terrain
	 * @param inGeometry Geometry where fill faces
	 * @param inWidth Width of the terrain
	 * @param inHeight Height of the terrain
	 */
	CreateFaces: function( inGeometry, inWidth, inHeight )
	{

		
		var indices = inGeometry.index;
		var id = 0;
		
		for( var y = 0; y < inHeight - 1; ++y )
		{
			for( var x = 0; x < inWidth - 1; ++x )
			{
				// First triangle
				indices[id ++] = y * inWidth + x + 1;
				indices[id ++] = y * inWidth + x;
				indices[id ++] = ( y + 1 ) * inWidth + x;
				
				// Second triangle
				indices[id ++] = ( y + 1 ) * inWidth + x + 1;
				indices[id ++] = y * inWidth + x + 1;
				indices[id ++] = ( y + 1 ) * inWidth + x;
			}
		}
	},
	
	/**
	 * Create geometry of the terrain from the given canvas and parameters
	 * @param inNoise 2D Canvas that store height informations
	 * @param inWidth Width of the terrain
	 * @param inHeight Height of the terrain
	 * @param inWidthSegments Number of segments on the width
	 * @param inHeightSegments Number of segments on the height
	 * @return The created geometry
	 */
	CreateGeometry: function( inNoise, inDepth, inWidth, inHeight, inWidthSegments, inHeightSegments )
	{
		var geometry = new THREE.BufferGeometry();
		
		var nbPoints = inNoise.width * inNoise.height;
		var indices = ( inNoise.width - 1 ) * ( inNoise.height - 1 ) * 2 * 3 ;
		geometry.setIndex(new THREE.BufferAttribute(new Uint32Array( indices ), 1) );
		geometry.addAttribute( 'color', new THREE.BufferAttribute(new Float32Array( nbPoints * 3 ), 3) );
		geometry.addAttribute( 'position', new THREE.BufferAttribute(new Float32Array( nbPoints * 3 ), 3) );
		


		this.CreateVertices( inNoise, geometry, inDepth, inWidth, inHeight );
		this.CreateFaces( geometry, inWidthSegments, inHeightSegments );
		
		return geometry;
	},
	
	ConstructTerrain: function( inNoise, inParameters )
	{		
		
		let geoSize = inParameters.width;
		let segments = inParameters.widthSegments;
		var geometry = new THREE.PlaneGeometry(geoSize, geoSize, segments, segments);

		var inWidth = inParameters.width;
		var inHeight = inParameters.height;
		var inDepth = inParameters.depth;

		var blockW = Math.floor(inNoise.width / segments);
		var blockH = Math.floor(inNoise.height / segments);
		//console.log(inWidth,inHeight,inDepth);



		var context = inNoise.getContext('2d'),
			imgData = context.getImageData( 0, 0, inNoise.width, inNoise.height ),
			pixels = imgData.data,
			scaleX = inWidth / ( inNoise.width - 1 ),
			scaleY = inDepth / 255,
			scaleZ= inHeight / ( inNoise.height - 1 ),
			id = 0,
			pixel = 0,
			offsetX = - inParameters.width / 2,
			offsetZ = - inParameters.height / 2;

			var data = new Uint16Array(pixels.length / 4);
			let d = 0;
				for (var p = 0; p < pixels.length; p+=4) {
					let r = pixels[p];
					let g = pixels[p+1];
					let b = pixels[p+2];

					let h = (r+g+b) / (255 * 3) * 255;

					data[d] = h;
					d++;
				}

function median(numbers) {
    // median of [3, 5, 4, 4, 1, 1, 2, 3] = 3
    var median = 0, numsLen = numbers.length;
    numbers.sort();
 
    if (
        numsLen % 2 === 0 // is even
    ) {
        // average of two middle numbers
        median = (numbers[numsLen / 2 - 1] + numbers[numsLen / 2]) / 2;
    } else { // is odd
        // middle number only
        median = numbers[(numsLen - 1) / 2];
    }
 
    return median;
}

				function avg(xStart,yStart,w,h){

					var avData = context.getImageData( xStart, yStart, w, h ).data;
					let pixels = 0;
					let tot = [];
					for( var y = 0; y < h; ++y )
					{
						for( var x = 0; x < w; ++x )
						{
							tot.push(avData[pixels] + avData[pixels+1] + avData[pixels+2]);
							pixels+= 4;
						}
					}

					return median(tot);


				}

//document.getElementById('noise').getContext("2d").putImageData( imgData, 0, 0, 0, 0, inWidth, inHeight );


	

		var offsetX = geometry.vertices[0].x;
		var offsetY = geometry.vertices[0].y;

		var cols = segments+1;
		var rows = segments+1;

		// Vertices are (segments +1) * (segments +1)

		// Geometry vertices build from bottom left

		// Canvas builts from top left



		for (var i = 0, l = (geometry.vertices.length); i < l; i++) {

			//console.log(i,geometry.vertices[i].x - offsetX,geometry.vertices[i].y - offsetY);
			let scale = inNoise.width/geoSize;
			let normalisedX = geometry.vertices[i].x - offsetX;
			let normalisedY = geometry.vertices[i].y - offsetY;
			
			let prog = i / geometry.vertices.length;
			let row = Math.floor(prog * rows);


			let pixelX = Math.abs(normalisedX * scale);
			let pixelY = inNoise.height + (normalisedY * scale); // Working from bottom up, so normalisedY is negative and added to the height to work up from teh bottom
			//console.log('row',row);
			let avSize = 1;

			let height = 0;
			let edge = false;

			pixelX = Math.min(pixelX,inNoise.width-1);
			pixelY = Math.min(pixelY,inNoise.height-1);

			pixelX = Math.max(pixelX,1);
			pixelY = Math.max(pixelY,1);

			if(pixelX == 0 || pixelX >= inNoise.width || pixelY >= inNoise.height || pixelY == 0) edge = true;



			if(edge){
				


				let pixelAvg = avg(pixelX,pixelY,1,1);
				height = (pixelAvg / (255 * 3)) * inDepth;

			}else{

				let pixelAvg = avg(pixelX,pixelY,avSize,avSize);
				height = (pixelAvg / (255 * 3)) * inDepth;

			}

			
			//if(pixelY <= 0) pixelY = 1;
			//if(pixelY >= inNoise.height) pixelY = inNoise.height;
			

	
		  //geometry.vertices[i].z = scaleY * ( pixels[ (pixel ++) * 4 + 1] );
		 //console.log(height);
		  geometry.vertices[i].z = height;
		}


		return geometry;

		// Create the corresponding geometry
		var geometry = this.CreateGeometry( inNoise, inParameters.depth, inParameters.width, inParameters.height, inParameters.widthSegments, inParameters.heightSegments );
		
		// Apply vertices effect
		for( var i = 0; i < inParameters.effect.length; ++i )
		{
			if( null !== inParameters.effect[i] )
				inParameters.effect[i].Apply( geometry, inParameters );
		}
		
		// Apply post algorithm as color generation
		for( var i = 0; i < inParameters.postgen.length; ++i )
		{
			if( null !== inParameters.postgen[i] )
				inParameters.postgen[i].Apply( geometry, inParameters );
		}
		
        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
	
		// Update the geometry
		geometry.attributes.color.needsUpdate = true;
		//geometry.index.needsUpdate = true;
		geometry.attributes.position.needsUpdate = true;

		return geometry;
	},
	
	/// ACCESSIBLE METHODS ///
	
	/**
	 * Generate a 2D Canvas from given parameters
	 * @return A canvas that store height map
	 */
	GetCanvas: function( inParameters )
	{
		inParameters = inParameters || {};

		// Manage default parameters
		inParameters.type = inParameters.type || 0;
		inParameters.depth = inParameters.depth || 10;
		inParameters.width = inParameters.width || 100;
		inParameters.height = inParameters.height || 100;
		inParameters.widthSegments = inParameters.widthSegments || 100;
		inParameters.heightSegments = inParameters.heightSegments || 100;
		inParameters.postgen = inParameters.postgen || [];
		inParameters.effect = inParameters.effect || [];
		inParameters.filter = inParameters.filter || [];
		
		if( typeof inParameters.canvas == 'undefined' )
			inParameters.canvas = this.CreateCanvas( inParameters.width, inParameters.height );
		inParameters.canvas.width = inParameters.widthSegments;
		inParameters.canvas.height = inParameters.heightSegments;

		
		var noise = inParameters.generator.Get( inParameters );
		
		// Apply filters
		for( var i = 0; i < inParameters.filter.length; ++i )
		{
			if( null !== inParameters.filter[i] )
				inParameters.filter[i].Apply( noise, inParameters );
		}
		
		return noise;
	},
	
	Get: function ( inParameters )
	{	
		

		return this.ConstructTerrain( this.GetCanvas( inParameters ), inParameters );
	},
	
	GetFromCanvas: function( inParameters, inCanvas, inX, inY, inWidth, inHeight )
	{

		// Extract a portion of the given canvas into an other
		var noise = this.CreateCanvas( inWidth, inHeight );
		var imageData = inCanvas.getContext("2d").getImageData( inX, inY, inWidth, inHeight );
		noise.getContext("2d").putImageData( imageData, 0, 0, 0, 0, inWidth, inHeight );
		
		var scaleWidth = inWidth / inParameters.widthSegments;
		var scaleHeight = inHeight / inParameters.heightSegments;
		var parameters = Object.create( inParameters );
		parameters.widthSegments = inWidth;
		parameters.heightSegments = inHeight;
		parameters.width = Math.floor( parameters.width * scaleWidth );
		parameters.height = Math.floor( parameters.height * scaleHeight );
		parameters.heightSegments = inHeight;
		
		return this.ConstructTerrain( noise, parameters );
	},
};

export default TERRAINGEN;