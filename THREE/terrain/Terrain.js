import * as THREE from 'three';

import RAND_DEFAULT from './import/randoms/default.js';
import RAND_MT from './import/randoms/mersenne-twister.js';

import PN_GENERATOR from './import/generators/perlinnoise.js';

import MOUNTAINS_COLORS from './import/colors/mountains.js';
import MOUNTAINS2_COLORS from './import/colors/mountains2.js';
import BLACKWHITE_COLORS from './import/colors/blackwhite.js';

import BLUR_FILTER from './import/filters/blur.js';
import GAMETERRAIN_FILTERS from './import/filters/gameterrain.js';
import CIRCLE_FILTER from './import/filters/circle.js';

import DEPTHNOISE_EFFECT from './import/verticeseffects/destructure.js';
import DESTRUCTURE_EFFECT from './import/verticeseffects/depthnoise.js';

import TERRAINGEN from './import/generators/terrain.js';

export default function Terrain(data){

	const defaults = {
		alea: RAND_MT,
		generator: PN_GENERATOR,
		width: 200,
		height: 200,
		widthSegments: 150,
		heightSegments: 150,
		depth: 300,
		param: 3,
		filterparam: 1,
		filter: [ BLUR_FILTER ],
		postgen: [ MOUNTAINS_COLORS ],
		effect: [ DEPTHNOISE_EFFECT ]
	};
	const params = {...defaults,...data};


	let group = new THREE.Group();



  var terrainGeo = TERRAINGEN.Get( params );
		var terrainMaterial = new THREE.MeshBasicMaterial( { color: 0xFFFFFF,vertexColors: THREE.VertexColors , side:THREE.DoubleSide, wireframe:true,flatShading: THREE.FlatShading } );
		var material = new THREE.MeshBasicMaterial( { color: 0xFFFFFF, wireframe: true } );
		var terrain = new THREE.Mesh( terrainGeo, terrainMaterial );

		terrain.rotation.x = Math.PI * -0.5;
		terrain.castShadow = true;
		terrain.receiveShadow = true;

		group.add(terrain);

		const clock = new THREE.Clock();
		let i = 0;

		const zPos = terrain.geometry.vertices.map((v) => v.z);
		const zWeight = terrain.geometry.vertices.map((v) => Math.random());
	
		function onRender(){

			const {geometry} = terrain;
			const now = clock.getElapsedTime();
			
			geometry.vertices.forEach(function(v,ix){

				var strength = zPos[ix] / (params.depth/2);
				var rand = Math.floor(Math.random() * 1) + 0.8  ;

				//v.z =  zPos[ix] + (Math.sin((strength * 0.5) * now) * 50);
				var adjust = (v.x + v.y) * 0.01;
				v.z =  zPos[ix] + (Math.sin(adjust + now) * (params.depth * 0.033) );

			});

			geometry.verticesNeedUpdate = true;
			
	

			//group.rotation.y += 0.005;

		}	

  return {object:group,mesh:terrain,onRender:onRender};
}

		

		
		