import * as THREE from 'three';
import Terrain from './terrain/Terrain.js';
import ImagePlane from './ImagePlane';

export default function NoiseFloor(data){


  //let world = World(data);
  let group = new THREE.Group();

 

   /*
    
        Terrain

    */

    var terrain = Terrain(data);
    group.add(terrain.object);

  	

	function onRender(){
    terrain.onRender();
		//imagePlane.object.rotation.y -= 0.001;
	}

  return {object:group,terrain:terrain,onRender:onRender};
}
