import * as THREE from 'three';


export default function Dla(){

    let geometry, pointcloud;
let nParticles = 120000, nextParticleIndex;
let spawnRadiusLimit = 140;
let sphere;
let container;
let doReset = false;
let worker = new Worker('/js/worker.js');

let params = {
    nParticlesPSec: 2000,
    particleSize: 1.0,
    displayFog: true,
    displayBoundary: false,
    displayDFrac: true,
    reset: registerReset
};



function registerReset(){
    doReset = true;
}

function registerReset(){
    doReset = true;
}

function reset(){
    doReset = false;
    for(var i=0; i<nParticles; ++i)
        pointcloud.geometry.vertices[i].fromArray([99999, 99999, 99999]);
    pointcloud.geometry.vertices[0].fromArray([0,0,0]);
    pointcloud.geometry.verticesNeedUpdate = true;
    nextParticleIndex = 1;

    params.reset = registerReset;
    worker.postMessage({ mode: 'init', spawnRadiusLimit: spawnRadiusLimit, nMax: nParticles });
    worker.postMessage({ mode: '', nParticlesPSec: params.nParticlesPSec });
}

    let group = new THREE.Group();


    let sGeometry = new THREE.SphereGeometry( spawnRadiusLimit-10, 16, 16 );
    let sMaterial = new THREE.MeshBasicMaterial( { color: 0xFFFFFF } );
    sphere = new THREE.EdgesHelper(  new THREE.Mesh( sGeometry, sMaterial ), 0xFFFFFF );
    sphere.visible = params.displayBoundary;

    group.add(sphere);

    // init point geometry
    geometry = new THREE.Geometry();
    for(var i=0; i<nParticles; ++i){
        geometry.vertices.push( new THREE.Vector3(99999,99999,99999) );
        geometry.colors[i] = new THREE.Color();
        //geometry.colors[i].setHSL( i/nParticles, 1, 0.5 );

        // HSL 328, 100%, 54%
        //let h = (360 * (1 + (i/nParticles)) % 360) / 360;
        geometry.colors[i].setHSL(  1, 1, 1  );

    }
    nextParticleIndex = 1;
    geometry.vertices[0].set(0,0,0);

    var sprite = THREE.ImageUtils.loadTexture( "/textures/gfx/sphere4.png" );
    //sprite.generateMipmaps = false;
    //sprite.minFilter = THREE.NearestFilter;
    //sprite.magFilter = THREE.NearestFilter;
    let material = new THREE.PointCloudMaterial( { size:5, mapOff: sprite, alphaTest: 0.5, vertexColors: THREE.VertexColors } );
    pointcloud = new THREE.PointCloud( geometry, material );
    group.add( pointcloud );
 

    // worker
    worker.onmessage = function(response) {
        if(doReset)
            reset();
        else if(response.data.type=="debug"){
            console.log(response.data.msg);
        }
        else if(!(response.data.goOn)){
            console.log("end detected.");
            params.reset = reset;
        }
        else{
            //console.log('here',response.data.positions);
            for(var i=0; i< response.data.positions.length; ++i){
                pointcloud.geometry.vertices[nextParticleIndex].fromArray(response.data.positions[i]);
                nextParticleIndex++;
            }


            pointcloud.geometry.vertices[1].set(1,-1,3);
            //console.log(pointcloud.geometry);
            pointcloud.geometry.verticesNeedUpdate = true;

            
            worker.postMessage({ mode: '', nParticlesPSec: params.nParticlesPSec });
        }
    };

    worker.postMessage({
        mode: 'init',
        spawnRadiusLimit: spawnRadiusLimit,
        nMax: nParticles });
    worker.postMessage({ mode: '', nParticlesPSec: params.nParticlesPSec });


    return {object:group,geometry:geometry,material:material};

}

