import * as THREE from 'three';
import Geometry from './Geometry';


// Return a THREEjs scene filled with some geometry

export default function Scene ( type="cube", numObjects=1000 ) {
  
  const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
  camera.position.z = 1200;
  
  // Setup scene
  const scene = new THREE.Scene();
  scene.add( new THREE.AmbientLight( 0x555555 ) );

  const light = new THREE.SpotLight( 0xffffff, 1.5 );
  light.position.set( 0, 500, 2000 );
  scene.add( light );

  const defaultMaterial = new THREE.MeshLambertMaterial({ color: 0xffffff, shading: THREE.FlatShading, vertexColors: THREE.VertexColors } );

  const mesh = new THREE.Mesh( Geometry( type, numObjects ), defaultMaterial );
  scene.add( mesh );

  return {scene,camera};
  
  
}

