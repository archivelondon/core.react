import React, { Component } from 'react';

import * as THREE from 'three';
import Particles from './Particles';
import ImageCloud from './ImageCloud';
import AccordingToChance from './AccordingToChance';

import CameraPath from './CameraPath';
import ImagePlane from './ImagePlane';

import NoiseFloor from './NoiseFloor';
import MillionDigits from './MillionDigits';
import RationalDivination from './RationalDivination';

import Terrain from './terrain/Terrain.js';
import Dla from './dla/Dla.js';

import SETTINGS from '../SETTINGS';

export default function TechnoSphere({data, images}){

    console.log('TECHNOSPHERE',data,images);
    const scene = new THREE.Scene();
    // Colour:
    scene.fog = new THREE.Fog( 0x1A1A1A, 1,5000);

    const max = -2000;
    const min = 2000;

    const minSize = 10;
    const maxSize = 20;


    /*

        Outer world

    */

    let sphere;


    var geometry = new THREE.SphereGeometry( 3000, 12, 12 );
    var material = new THREE.MeshBasicMaterial( { color: 0xFFFFFF, wireframe: true } );
    sphere = new THREE.Mesh( geometry, material );
    scene.add( sphere );




    /*

        Noise Floor

    */


    var noiseFloor = NoiseFloor(data[0]);
    scene.add(noiseFloor.object);

    noiseFloor.object.position.x = 1000;
    noiseFloor.object.position.y = 400;
    noiseFloor.object.position.z = 1500;
    noiseFloor.object.rotation.x = -100;
    noiseFloor.object.rotation.z = -50;

    var terrain = noiseFloor.terrain;


    /*

        Million Randon Digits

    */

    var millionDigits = MillionDigits(data[1]);
    scene.add(millionDigits.object);

    millionDigits.object.position.x = -1500;
    millionDigits.object.position.y = 200;
    millionDigits.object.position.z = -1000;
    millionDigits.object.rotation.x = 0;

    /*

        Rational Divination

    */

    //var rationalDivination = RationalDivination(data[2]);
    //scene.add(rationalDivination.object);

    //rationalDivination.object.position.x = 0;
    //rationalDivination.object.position.y = 200;
    //rationalDivination.object.position.z = -1800;
    //rationalDivination.object.rotation.x = -20;
    //rationalDivination.object.rotation.z = -20;


    /*

        2048 according to chance

    */


    let accordingToChance = AccordingToChance(data[SETTINGS.ACCORDINGTOCHANCE_KEY].content.slice(0,5));

    accordingToChance.object.position.set(-2450,1720,-4700)
    //accordingToChance.object.rotation.x = Math.PI * 23;
    scene.add(accordingToChance.object);
    //if(!window.imageHistory) window.imageHistory = imageHistory;


    /*
        
        Texture Sphere - TODO move to own function

    */

    let sphereG = new THREE.SphereGeometry( 1000, 20, 20 );
    let sphereMat = new THREE.MeshBasicMaterial( { color:0xFFF000, wireframe:true } );
    let sphereMesh = new THREE.Mesh( sphereG, sphereMat );

    scene.add( sphereMesh );
    sphereMesh.position.set(-2000,2000,-5100);

    const updateSphereTexture = (src) => {

        let loader = new THREE.TextureLoader();
        loader.load( src, function ( texture ) {


        let loadedMaterial = new THREE.MeshBasicMaterial( { side:THREE.BackSide, map: texture, overdraw: 0.5 } );
        sphereMesh.material = loadedMaterial;

        sphereMesh.material.needsUpdate = true;


    } );
    }


        const revertSphereTexture = () => {

       sphereMesh.material = sphereMat;

        sphereMesh.material.needsUpdate = true;


    }

    

    /*

        Image particles

    */

    let imageCloud;

    const particles = Particles();
    particles.object.scale.x = 5;
    particles.object.scale.y = 5;
    particles.object.scale.z = 5;
    scene.add(particles.object);

    const cameraPath = CameraPath();
    scene.add(cameraPath.object);


    const onRender = function(){
        noiseFloor.object.rotation.y += 0.001;
        noiseFloor.object.rotation.x += 0.001;
        millionDigits.object.rotation.y -= 0.001;
        sphere.rotation.y += 0.001;
        particles.onRender();
        noiseFloor.onRender();
        sphereMesh.rotation.x += 0.0007;
        
        accordingToChance.imagePlanes.forEach(function(imagePlane) {
            
            imagePlane.object.position.z = imagePlane.object.orig.z + ((Math.sin(((imagePlane.object.orig.x * 2) + Date.now()) * 0.001) * 30));

        });

    }

    return {terrain,scene,data,particles,imageCloud,accordingToChance,sphere,noiseFloor,millionDigits,cameraPath,onRender,updateSphereTexture,revertSphereTexture};

}

