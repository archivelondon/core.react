import * as THREE from 'three';

export default function ImagePlane(obj,boundsX=2000,boundsY=2000,boundsZ=2000){

            const texLoader = new THREE.TextureLoader();
            const texture = texLoader.load(obj.image);

            

            var w = obj.width || 200;
            var h = obj.height || 200;

            const geometry = new THREE.PlaneGeometry( w, h, 1 );
            const material = new THREE.MeshBasicMaterial( {map: texture,color:0xCCCCCC,side: THREE.DoubleSide,transparent:true,opacity:1,wireframe: false} );

           let plane = new THREE.Mesh(geometry, material);

            plane.position.x = Math.random() * boundsX;
            plane.position.y = Math.random() * boundsY;
            plane.position.z = Math.random() * boundsZ;


            const onRender = function() {

                plane.rotation.x += 0.0001;

            }

    return {object:plane, onRender};
}
