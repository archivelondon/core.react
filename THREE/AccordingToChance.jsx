import * as THREE from 'three';
import ImagePlane from './ImagePlane';

export default function AccordingToChance(images,boundsX=600,boundsY=300,boundsZ=50){

    let group = new THREE.Group();
    let imagePlanes = [];
    let ids = {};

    if(images) {
      images.map((obj,i) => {

        if( obj.image != undefined ) {
          const imagePlane = ImagePlane(obj,boundsX,boundsY,boundsZ);
          imagePlanes.push(imagePlane);
          imagePlane.object.position.x = (i * 250); 
          imagePlane.object.position.z = 0 ;
          imagePlane.object.orig = {...imagePlane.object.position};
          group.add(imagePlane.object);
        }
      });
    }

    const showImage = (id) => {



    }

    return {object:group, ids, imagePlanes, showImage};
}
