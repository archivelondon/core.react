import * as THREE from 'three';
import ImagePlane from './ImagePlane';

export default function ImageHistory(images,boundsX,boundsY,boundsZ){

    let group = new THREE.Group();
    let imagePlanes = [];

    if(images) {
      images.map((obj) => {

        if( obj.image != undefined ) {
          const imagePlane = ImagePlane(obj,boundsX,boundsY,boundsZ);
          imagePlanes.push(imagePlane);
          group.add(imagePlane.object);
        }
      });
    }

    return {object:group, imagePlanes};
}
