import React, { Component } from 'react';
import * as THREE from 'three';

var OrbitControls = require('three-orbit-controls')(THREE)

class ThreeJs extends Component {

paused = 0;

constructor(props) {
    super(props);
    this.state = {};
    this.objects = {};
  }

componentDidMount() {

	this.init();

  window.addEventListener('resize',this.resize);

}

componentWillUnmount() {
    this.stopLoop();
    window.removeEventListener('resize',this.resize);
  }

resize = () =>{

    const {renderer,camera} = this.objects;
    camera.aspect = this.refs.canvas.offsetWidth / this.refs.canvas.offsetHeight;
    camera.updateProjectionMatrix();
    //console.log('resized camera',this.refs.canvas.offsetWidth,this.refs.canvas.offsetHeight);

    renderer.setSize( this.refs.canvas.offsetWidth, this.refs.canvas.offsetHeight );

}

init() {

	   const scene = new THREE.Scene();
 
    const camera = new THREE.PerspectiveCamera( 75, this.refs.canvas.offsetWidth / this.refs.canvas.offsetHeight, 1, 10000 );
    camera.position.z = 1000;
    
    let controls;
    if(this.props.noControls){
        // DUmmy controls for compatibility
        controls = {
          update : function(){}
        };
    }else{
        controls = new OrbitControls(camera,this.refs.canvas);
        controls.target = new THREE.Vector3(0,0,0);
    }
 
    const renderer = new THREE.WebGLRenderer( { alpha:true, antialias: true } );
    renderer.setClearColor( 0x000000 ,0);
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( this.refs.canvas.offsetWidth, this.refs.canvas.offsetHeight );
    this.refs.canvas.appendChild( renderer.domElement );

    let play = () => {
      this.paused = 0;
      this.loop();
    }

   let pause = () => {
    this.paused = 1;
    this.stopLoop();
   }

   let render = () => {
    this.renderScene();
   }

	 this.objects = {scene,camera,renderer,controls,play,pause,render}

   

    // Add any arbitrary objects sent in as 'objects' prop
   //<ThreeJs objects={{foo:'bar'}} />
   if(this.props.objects){
      Object.assign(this.objects,this.props.objects);
   }

   if(this.props.onInit){
      const objectsAdded = this.props.onInit(this.objects);
      if(objectsAdded) Object.assign(this.objects,objectsAdded);
    }

    
    this.resize();
    if(!this.paused) requestAnimationFrame(this.loop);

}
  
  stopLoop = () => {
    cancelAnimationFrame(this.loop);
  }
  rendering = false;
  renderScene = () => {

    if(this.rendering) return;
    this.rendering = true;
    const {renderer,scene,camera} = this.objects

    if(this.props.onLoop) this.props.onLoop(this.objects);

    if(this.props.render) this.props.render(this.objects)
    else renderer.render(scene,camera);
  
    this.rendering = false;
  }

  loop = () => {
    
  	this.renderScene();
    
    requestAnimationFrame(this.loop);

  }


  render() {
    return (
      <div style={{width:'100%',height:'100%'}} ref="canvas"></div>
    );
  }
}

export default ThreeJs;
