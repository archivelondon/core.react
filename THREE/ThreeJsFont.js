import React, { Component } from 'react';
import * as THREE from 'three';

class ThreeJsFont extends Component {

font;

constructor(props) {
    super(props);
    this.state = {loaded:false,progress:0,error:false};
  }

componentDidMount() {

	this.init();

}


init = () =>{
    let _this = this;
    
    if(!this.props.src){
         _this.setState({error:'No font src'});
         return;
    };

    var loader = new THREE.FontLoader();
    loader.load( this.props.src, function ( font ) {

        _this.font = font;
        _this.setState({loaded:true});

    },
    function(xhr){
      let total = xhr.total;
      if(!total){
        total = _this.props.total || 200000; // Assume 200kb or allow props to set a specific total
      }
      let prog = (xhr.loaded / total);
      //console.log('THREE.FontLoader progress',prog,xhr);
      _this.setState({progress:prog});

    },function(err){

      //console.log('THREE.FontLoader error',err);
      _this.setState({error:'Error Loading Font'});

    });
}

  render() {
    if(this.state.loaded){
      
      return this.props.children({font:this.font});
    }else if (this.state.error){
      return <div>{this.state.error}</div>
    }else{
      return null
    }
  }
}

export default ThreeJsFont;
