import * as THREE from 'three';
import World from './World';
import Dla from './dla/Dla';

export default function NoiseFloor(data){

	let group = new THREE.Group();
  	//let world = World(data);

      var dla = Dla();

     dla.object.scale.set(10,10,10);
    group.add(dla.object);

  return {object:group,dla:dla};
}
