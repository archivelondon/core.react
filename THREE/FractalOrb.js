import * as THREE from 'three';

export default function FractalOrb(){

            

        let vertexShader = [
            'attribute float vertexDisplacement;',
            'uniform float delta;',
            'varying float vOpacity;',
            'varying vec3 vUv;',
            'void main() ',
            '{',
                'vUv = position;',
                'vOpacity = vertexDisplacement;',
                'vec3 p = position;',
                'p.x += sin(vertexDisplacement) * 20.0;',
                'p.y += cos(vertexDisplacement) * 20.0;',
                'vec4 modelViewPosition = modelViewMatrix * vec4(p, 1.0);',
                'gl_Position = projectionMatrix * modelViewPosition;',
            '}',
        ].join('\n');

        let fragmentShader = [
            'uniform float time;',
            'varying float vOpacity;',
            'varying vec3 vUv;',
            'void main() {',
                'float r = 0.5 + sin(vUv.x * time);',
                'float g = 0.5 + sin(vUv.x * time);',
                'float b = 0.5 + sin(vUv.x * time);',
                'vec3 rgb = vec3(r, g, b);',
                'gl_FragColor = vec4(rgb, 0.2);',
            '}'
        ].join('\n');

        let textureLoader = new THREE.TextureLoader();
        let geometry = new THREE.SphereGeometry( 2000, 100,100 );
        let uniforms = {
                    time: { value: 1.0 },
                };



        let material = new THREE.ShaderMaterial( {
            uniforms: uniforms,
            vertexShader: vertexShader,
            fragmentShader:fragmentShader,
            side: THREE.BackSide
        } );



        let mesh = new THREE.Mesh( geometry, material );
                //mesh.rotation.x = 20;
                mesh.rotation.z = Math.PI * -0.15;
                
            

            const onRender = function() {
                    mesh.rotation.z += 0.001;
                    //mesh.scale.y += 0.00011;
                    //uniforms.time.value += 0.0001;
                    mesh.geometry.verticesNeedUpdate = true;

            }

    return {object:mesh, onRender};
}
