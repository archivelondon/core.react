import React, { Component } from 'react';
import * as THREE from 'three';
import TechnoSphere from './TechnoSphere';
import Particles from './Particles';
import MetaBalls from './MetaBalls';
import ImageCloud from './ImageCloud';
import ImageHistory from './ImageHistory';
import FractalOrb from './FractalOrb';

import SETTINGS from '../SETTINGS';

let ambientColor = 0x31227A;
ambientColor = 0x60FFDD;
ambientColor = 0x283F84;

let highlightColor = 0x60FFDD;
highlightColor = 0x00ff00;
highlightColor = 0xF534BD;
highlightColor = 0xEC4935;

let highlightColor2 = 0x60FFDD;
highlightColor2 = 0xFFFF00;

export default function TechnoSphereColoured({data, images}){

    // Build our scene from the technosphere scene
    const {scene,objs,particles,sphere,noiseFloor,terrain,millionDigits} = TechnoSphere({data});

    // Colour:
    scene.fog = new THREE.Fog( 0x1A1A1A, 1,2500);
    // Pink fog
    scene.fog.color.setRGB(1,0.7,0.7);
    scene.add( new THREE.AmbientLight( 0xDDDDDD ) );

    const light = new THREE.SpotLight( highlightColor, 1 );
    light.position.set( 0, 3000, 0 );
    scene.add( light );

    const light3 = new THREE.SpotLight( highlightColor2, 0.2 );
    light3.position.set( -3000, 1000, -1000 );
    scene.add( light3 );

    const light2 = new THREE.SpotLight( ambientColor, 2 );
    light2.position.set( 3000, 1000, 0 );
    scene.add( light2 );

    // Coloured materials:
    var materialColoured = new THREE.MeshLambertMaterial({ color: 0x60FFDD, wireframe:false, shading: THREE.SmoothShading, side:THREE.BackSide  ,vertexColors: THREE.VertexColors } );

    // Change big sphere
    sphere.material = materialColoured;

    // Make small sphere smooth geometry and update texture
    var geometry2 = new THREE.SphereGeometry( 400, 32, 32 );
    //noiseFloor.sphere.material = materialColoured;
    //noiseFloor.sphere.geometry = geometry2;

    const noiseFloorParticles = Particles();
    scene.add(noiseFloorParticles.object);
    noiseFloorParticles.object.position.set(1500,400,1500);

   const metaBalls = MetaBalls();
    scene.add(metaBalls.object);

    let blockerG = new THREE.PlaneGeometry( 1000, 1500, 6 );
    let blockerM = new THREE.MeshBasicMaterial( { color: 0xFF0000, wireframe: false,side:THREE.BackSide } );
    blockerM.color.setRGB(1,0.7,0.7); // Same as fog
    let blockerS = new THREE.Mesh( blockerG, blockerM );
    blockerS.position.set(0,550,-50);
    scene.add(blockerS);
   
    metaBalls.object.position.set(-2500,500,-500);
    
    //metaBalls.object.position.set(-1500,200,-800);

    var terrainMat = new THREE.MeshNormalMaterial({ color: 0x999999, shininess:0,wireframe:false, shading: THREE.SmoothShading, side:THREE.DoubleSide } );
    //var normalMat = new THREE.MeshLambertMaterial( { color: 0xFFFFFF,wireframe:false,vertexColors: THREE.VertexColors , side:THREE.DoubleSide,shading: THREE.FlatShading,});
    terrain.mesh.material = terrainMat;
    terrain.mesh.geometry.verticesNeedUpdate = true;
    terrain.mesh.geometry.computeFaceNormals();
    terrain.mesh.geometry.computeVertexNormals();


    const imageCloud = ImageCloud();
    imageCloud.object.position.set(1000,400,800);
    scene.add(imageCloud.object);

    let outro = ImageHistory(images,2000,5000,200);
    let outro2 = ImageHistory(images,5000,10000,100); //smaller and slower

    let outroStart = SETTINGS.COORDS[4].y + 1500 ;
    let outroEnd = outroStart - 7000;

    let outro2Start = outroStart + 500;
    let outro2End = outro2Start - 10000;


    outro.object.position.set(SETTINGS.COORDS[4].x -900,outroStart, SETTINGS.COORDS[4].z-500)
    outro2.object.position.set(SETTINGS.COORDS[4].x -2500 ,outro2Start, SETTINGS.COORDS[4].z-400)
    scene.add(outro.object);
    scene.add(outro2.object);

    const fractalOrb = FractalOrb();
    scene.add(fractalOrb.object);

    //const metaBalls2 = MetaBalls();
    //scene.add(metaBalls2.object);
    //metaBalls2.object.material.color.setRGB( 0.7,1,0 );


    //metaBalls2.object.position.set(SETTINGS.COORDS[4].x,SETTINGS.COORDS[4].y + 1500,SETTINGS.COORDS[4].z - 1500 );
    fractalOrb.object.position.set(SETTINGS.COORDS[4].x,SETTINGS.COORDS[4].y + 1800,SETTINGS.COORDS[4].z + 300);
    


    millionDigits.dla.material.transparent = true;
    millionDigits.dla.material.opacity = 0.85;

    let outro2Speed = 1;
    let outroSpeed = 3;
    let paused = false;
    const pause = () => {

        if(!paused){
            paused = true;
            outro2Speed = 0.2;
            outroSpeed = 0.5;
        }else{
            paused = false;
            outro2Speed = 1;
            outroSpeed = 3;

        }
        

    }

    const onRender = function(){

        let time = Date.now() * 0.00005;
        metaBalls.onRender();
        //metaBalls2.onRender();
        particles.onRender();
        noiseFloorParticles.onRender();
        noiseFloor.object.rotation.y += 0.001;
        noiseFloor.object.rotation.x += 0.0006;
        noiseFloor.onRender();
        terrain.onRender();


        imageCloud.object.position.y = 450 + ((Math.sin(Date.now() * 0.0005)) * 20);
        imageCloud.object.rotation.y = (Math.PI * -0.1) + (Math.PI * ((Math.sin(Date.now() * 0.0005)) * 0.1)); 

        let h = (360 * (1 + time) % 360) / 360;

        millionDigits.dla.geometry.colors.forEach((item,ix) => {

                item.setHSL( (ix/millionDigits.dla.geometry.colors.length)  , 1, 0.5 );
        })
        
        outro2.object.position.y -= outro2Speed;
        if(outro2.object.position.y < outro2End){
            outro2.object.position.y = outro2Start;
            //outro2Speed *= 2;
        }

        outro.object.position.y -= outroSpeed;
        if(outro.object.position.y < outroEnd){
            outro.object.position.y = outroStart;
            //outroSpeed *= 2;
        }

        //millionDigits.dla.material.color.setHSL(h, 0.8, 0.5);
        millionDigits.dla.object.rotation.y += 0.002;
        
        //fractalOrb.object.scale.x = 1 + (Math.sin(time));
        fractalOrb.onRender();

    }

    return {scene,objs,data,particles,sphere,noiseFloor,millionDigits,onRender,imageCloud,pause};

}

