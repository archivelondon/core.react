import React, { Component } from "react";

import { TweenMax, Sine, Expo, Power1, Power0 } from "gsap";
Math.easeOutQuad = function(t, b, c, d) {
  t /= d;
  return -c * t * (t - 2) + b;
};

const USE_NATIVE = true; // Override for just using the native scroll

class MouseWheel extends Component {
  lastEventTime = 0; // When the last non-throttled event fired
  lastWheelTime = 0; // When the last native mouseheel event fired
  lastDelta = 0;
  interval; // scroll is being eased
  mult = 0; // how fast do we scroll
  dir = 0; // 1 = scroll down, -1 = scroll up
  steps = 2; // how many steps in animation
  length = 30; // how long to animate
  throttle = 1500; // throttle the wheel event
  animation;
  highestDelta = 0;
  evCount = 0;

  // Touch

  touchTime = 0;
  touchPos = { x: 0, y: 0 };
  touchStart = { x: 0, y: 0 };
  touchMoved = { x: 0, y: 0 };
  touchEnded = 0;
  moving = 0;

  constructor(props) {
    super(props);
    this.state = { pos: 0, delta: 0 };
  }

  componentDidMount() {
    this.direction = this.props.direction && this.props.direction == "h" ? "x" : "y";
  }

  getDir = () => {
    return this.props.direction && this.props.direction == "h" ? "x" : "y";
  }

  touchStart = e => {
    if (this.animation) this.animation.kill();

    let d = new Date();
    this.touchTime = d.getTime();

    this.touchStart.x = e.touches[0].pageX;
    this.touchStart.y = e.touches[0].pageY;
    this.touchPos.x = e.touches[0].pageX;
    this.touchPos.y = e.touches[0].pageY;
    this.touchMoved = { x: 0, y: 0 };
  };

  touchMove = e => {
    const _this = this;
    let xMoved = this.touchPos.x - e.touches[0].pageX;
    let yMoved = this.touchPos.y - e.touches[0].pageY;

    let pos,diff


    if (this.getDir() == "y") {
      if (yMoved != 0) {
        e.preventDefault();
        //e.stopPropagation();
      } else {
        // Not moving the y axis
        return;
      }

      this.touchPos.y = e.touches[0].pageY;

      pos = this.state.pos;
      diff = yMoved * (this.props.touchSensitivity || 1);

      pos += yMoved;
    } else {
      if (xMoved != 0) {
        e.preventDefault();
        //e.stopPropagation();
      } else {
        // Not moving the y axis
        return;
      }

      this.touchPos.x = e.touches[0].pageX;

      pos = this.state.pos;
      diff = xMoved * 0.4;

      pos += xMoved;
    }


    if (_this.props.onUpdate) {
      _this.props.onUpdate({ delta: diff, pos: pos, e: e, type: "touch" });
    }

    _this.setState({ pos: pos, delta: diff });
  };

  touchEnd = e => {
    let _this = this;
    let d = new Date();
    let releaseTime = d.getTime();
    let moved, start, end, maxDuration;

    let duration = releaseTime - this.touchTime;

    console.log(this.getDir());

    if (this.getDir() == "x") {
      moved = this.touchPos.x - this.touchStart.x;
      start = this.touchPos.x;
      end = this.touchPos.x + moved * (this.props.touchSensitivity || 0.4);
      maxDuration = 1000;
      console.log(moved, duration)
    } else {
      moved = this.touchPos.y - this.touchStart.y;
      start = this.touchPos.y;
      end = this.touchPos.y + moved * (this.props.touchSensitivity || 0.4);

      maxDuration = 300;

    }

    const anim = { val: start };

    if (duration < maxDuration && Math.abs(moved) > 2) {
      _this.animation = TweenMax.to(anim, 1, {
        val: end,
        ease: Expo.easeOut,
        onUpdate: function() {
          var diff = anim.val - end;
          //console.log('ease',diff);

          if (_this.props.onUpdate) {
            _this.props.onUpdate({
              delta: diff,
              pos: anim.val,
              e: e,
              type: "touch"
            });
          }

          _this.setState({ pos: anim.val, delta: diff }); // scroll the target to next step
        },
        onComplete: function() {
          _this.mult = 1;
        }
      });
    }
  };

  listen = e => {
    const _this = this;

    e.preventDefault(); // prevent default browser scroll
    e.stopPropagation();

    this.wheel(e);
  };

  wheel = e => {
    const _this = this;
    _this.evCount++;

    // Work out when we last fired the event
    const now = new Date().getTime();
    const timeSince = Math.abs(now - _this.lastEventTime);
    const timeSinceWheel = Math.abs(now - _this.lastWheelTime);

    const swipe = timeSinceWheel > 200 ? true : false;

    _this.lastWheelTime = now;

    const useDelta =
      this.props.direction && this.props.direction == "h"
        ? e.wheelDeltaX
        : e.wheelDeltaY;

    const firstEvent = !_this.lastEventTime;

    // Store original delta and an absolute value to se if it has amplified
    var origDelta = useDelta || -e.detail;

    _this.highestDelta = Math.max(_this.highestDelta, Math.abs(origDelta));

    var absDelta = Math.abs(origDelta);
    //var moreDelta = (absDelta > (_this.highestDelta * 0.25)) && (timeSince > 400) ;

    var moreDelta =
      (absDelta > _this.lastDelta ||
        (absDelta < 3 && absDelta >= _this.lastDelta)) &&
      timeSince > 500 &&
      _this.evCount > 2;

    _this.lastDelta = absDelta;

    const deltaDiff = Math.abs(absDelta - _this.lastDelta);
    const largeDiff = deltaDiff > 150;
    const regularEvent = timeSince < 600;

    const alreadySmoothScrolling = !largeDiff && regularEvent;
    //if(!alreadySmoothScrolling) console.log('smooth?',alreadySmoothScrolling,'big dif',largeDiff,deltaDiff,'regular',regularEvent,timeSince);

    // Already smooth scrolling are teh cases where the event is firing regularly and the difference between the values is quite small

    if (alreadySmoothScrolling || USE_NATIVE) {
      let pos = this.state.pos;
      let diff = -origDelta;
      _this.lastEventTime = now;
      pos += origDelta;
      if (_this.props.onUpdate) {
        _this.props.onUpdate({
          delta: diff,
          pos: pos,
          e: e,
          swipe: swipe,
          type: "wheel"
        });
      }
      _this.setState({ pos: pos, delta: diff });

      return;
    }

    /*
  --------------------

  */

    // Normalise the delta
    var delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail)); // cross-browser

    // Log if direction changed
    const changedDirection = _this.dir != delta;

    const throttleExpired = timeSince > _this.throttle;

    /* 
    We allow the scroll to update if:
  

    - scroll direction changed
    - enough time has passed since a throttled event
    - the delta increased from the original event

  */

    if (changedDirection || throttleExpired || moreDelta) {
      // Update the last event time (non-throttled event)
      _this.lastEventTime = now;

      // Get state pos...
      let currentPos = _this.state.pos;

      if (changedDirection) {
        // scroll direction changed

        _this.mult = 1; // start slowly
        _this.dir = delta;

        clearInterval(_this.interval); // cancel previous animation
        if (_this.animation) _this.animation.kill();

        //if(!firstEvent) return;
      } else {
        // Scroll direction hasn't changed, so we increase the delta
        // Each time the wheel is called, we will increase the multiplier
        _this.mult += 0.5;
      }

      clearInterval(_this.interval); // cancel previous animation

      var start = currentPos;
      var end = start + _this.length * _this.mult * delta; // where to end the scroll
      var change = end - start; // base change in one step
      var step = 0; // current step

      const anim = { val: start };

      if (_this.animation) _this.animation.kill();

      let durationMultiplier = _this.mult <= 1 ? 1 : 1;
      const duration = 1 * _this.mult;

      _this.animation = TweenMax.to(anim, duration, {
        val: end,
        ease: Expo.easeOut,
        onUpdate: function() {
          var diff = anim.val - end;
          //console.log('ease',diff);

          if (_this.props.onUpdate) {
            _this.props.onUpdate({ delta: diff, pos: anim.val, e: e });
          }

          _this.setState({ pos: anim.val, delta: diff }); // scroll the target to next step
        },
        onComplete: function() {
          _this.mult = 1;
        }
      });

      /*_this.interval = setInterval(function() {
    var pos = Math.easeOutQuad(step++,start,change,_this.steps); // calculate next step
    var diff = pos - end;
    
    console.log('wheel',timeSince,diff,_this.mult);
    
    if(_this.props.onUpdate){

        _this.props.onUpdate({delta:diff,pos:pos,e:e});

    }

    _this.setState({pos: pos,delta:diff}); // scroll the target to next step
    
    if(step>=_this.steps) { // scroll finished without speed up - stop animation
      _this.mult = 0; // next scroll will start slowly
     
      clearInterval(_this.interval);
    }
  },10);*/
    }
  };

  componentDidMount() {
    let el = this.refs.el;

    // nonstandard: Chrome, IE, Opera, Safari
    el.addEventListener("mousewheel", this.listen, false);
    // nonstandard: Firefox
    el.addEventListener("DOMMouseScroll", this.listen, false);

    el.addEventListener("touchstart", this.touchStart, false);
    el.addEventListener("touchmove", this.touchMove, false);
    el.addEventListener("touchend", this.touchEnd, false);
  }

  componentWillUnmount() {
    let el = this.refs.el;

    // nonstandard: Chrome, IE, Opera, Safari
    el.removeEventListener("mousewheel", this.listen);
    // nonstandard: Firefox
    el.removeEventListener("DOMMouseScroll", this.listen);
  }

  render() {
    return (
      <div {...this.props} ref="el">
        {this.props.children}
      </div>
    );
  }
}

export default MouseWheel;