import React, { Component } from 'react';
import PubSub from 'pubsub-js';

const withEvents = (WrappedComponent, listeners, triggers) => {
  return class EventComponent extends Component {
    evTokens = {};

    constructor(props) {
      super(props);
      window.PubSub = PubSub;
  
    }

  componentDidMount() {

      const _this = this;

      for(let label in listeners){
          if(listeners.hasOwnProperty(label)){
            _this.evTokens[label] = PubSub.subscribe( label, (label,data) => {
                
                listeners[label].call(_this.refs.WrappedComponent,data);

            });
          }
        }



  }

  subscribe = (label,callback) => {

      this.evTokens[label] = PubSub.subscribe( label, (label,data) => callback(data) );

  }

  trigger = (label,data) => {
  	
      PubSub.publish(label,data);

  }


  componentWillUnmount() {


       for(let label in listeners){
          if(this.evTokens.hasOwnPropert(label)){
            PubSub.unsubscribe( this.evTokens[label] );
          }
        }
    }


    render() {

      return (
        <WrappedComponent ref="WrappedComponent" subscribe={this.subscribe} trigger={this.trigger} { ...this.props } />
      );
    }
  }
}

export default withEvents;