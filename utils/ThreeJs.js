import React, { Component } from 'react';
import * as THREE from 'three';

class ThreeJs extends Component {

stopped = 0;
ticker;

constructor(props) {
    super(props);
    this.state = {};
    this.objects = {};
  }

componentDidMount() {

	this.init();

  window.addEventListener('resize',this.resize);

}

componentWillUnmount() {
    this.stopLoop();
    window.removeEventListener('resize',this.resize);
  }

resize = () =>{

    const {renderer,camera} = this.objects;

    camera.aspect = (this.props.aspect) ? this.props.aspect : this.refs.canvas.offsetWidth / this.refs.canvas.offsetHeight;
    camera.updateProjectionMatrix()

    renderer.setSize( this.refs.canvas.offsetWidth, this.refs.canvas.offsetHeight );

}

init() {

	const scene = new THREE.Scene();
    
    const aspect = (this.props.aspect) ? this.props.aspect : this.refs.canvas.offsetWidth / this.refs.canvas.offsetHeight;
    const fov = (this.props.fov) ? this.props.fov : 75;
    const near = (this.props.near) ? this.props.near : 1;
    const far = (this.props.far) ? this.props.far : 10000;

    const camera = new THREE.PerspectiveCamera( fov,aspect, near, far );
    camera.position.z = 1000;
 
 
    const renderer = new THREE.WebGLRenderer( { alpha:true, antialias: true } );
    renderer.setClearColor( 0x000000 ,0);
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( this.refs.canvas.offsetWidth, this.refs.canvas.offsetHeight );
 

	 this.objects = {scene,camera,renderer}

   if(this.props.onInit){
      const objectsAdded = this.props.onInit(this.objects);
      if(objectsAdded) Object.assign(this.objects,objectsAdded);
    }

    this.refs.canvas.appendChild( renderer.domElement );
    
    requestAnimationFrame(this.loop);

}

  loop = () => {
    
    if(this.stopped) return;
  	const {renderer,scene,camera} = this.objects


    if(this.props.onLoop) this.props.onLoop(this.objects);

    renderer.render(scene,camera);
    
    this.ticker = requestAnimationFrame(this.loop)
  }

  stopLoop = ()=> {
    this.stopped = 1;
    cancelAnimationFrame(this.ticker);


  }


  render() {
    return (
      <div style={{width:'100%',height:'100%'}} ref="canvas"></div>
    );
  }
}

export default ThreeJs;
