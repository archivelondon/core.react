import React, { Component } from 'react';


let DATA = {};

const withKey = (WrappedComponent, ix) => {
  return class Iteration extends Component {

    constructor(props) {
      super(props);
  
    }

  componentDidMount() {


  }



  componentWillUnmount() {



    }


    render() {

      return (
        <WrappedComponent ref="WrappedComponent" key={ix} { ...this.props } />
      );
    }
  }
}

export default withKey;